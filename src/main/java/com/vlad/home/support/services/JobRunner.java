package com.vlad.home.support.services;

import com.vlad.home.model.User;
import com.vlad.home.services.UserService;
import com.vlad.home.support.exception.ShedulerExistException;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * Created by Vlad Z
 */
@Component
public class JobRunner {

    @Autowired
    private UserService userService;

    private final StdSchedulerFactory factory = new StdSchedulerFactory();
    private final Scheduler scheduler = factory.getScheduler();


    public JobRunner() throws SchedulerException {}

    public void runScheduler(Service service) throws ShedulerExistException {

        org.springframework.security.core.userdetails.User principal = (org.springframework.security.core.userdetails.User)
                SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userService.getUserByEmail(principal.getUsername());

        Map<String, Object> data = new HashMap<>();
        data.put("service", service);
        data.put("user", user);


        JobDetail job1 = newJob(EmailJobExecutor.class)
                .withIdentity("job1", "group1")
                .setJobData(new JobDataMap(data))
                .build();


        Trigger trigger = newTrigger()
                .withIdentity("trigger3", "group1")
                .startNow()
                .withSchedule(simpleSchedule()
                        .withIntervalInMinutes(60)
                        .repeatForever())
                .build();

        // Schedule the job with the trigger
        try {
            scheduler.scheduleJob(job1, trigger);
            scheduler.start();
        } catch (SchedulerException e) {
            throw new ShedulerExistException(e.getMessage(), "'job1 group1'");
        }


    }

}
