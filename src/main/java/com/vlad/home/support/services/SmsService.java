package com.vlad.home.support.services;

import com.vlad.home.model.User;
import org.jsmpp.InvalidResponseException;
import org.jsmpp.PDUException;
import org.jsmpp.bean.*;
import org.jsmpp.examples.SimpleSubmitExample;
import org.jsmpp.extra.NegativeResponseException;
import org.jsmpp.extra.ProcessRequestException;
import org.jsmpp.extra.ResponseTimeoutException;
import org.jsmpp.session.*;
import org.jsmpp.util.AbsoluteTimeFormatter;
import org.jsmpp.util.InvalidDeliveryReceiptException;
import org.jsmpp.util.TimeFormatter;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Date;

/**
 * Created by Vlad Z
 */
@Component
public class SmsService implements Service {

    private static TimeFormatter timeFormatter = new AbsoluteTimeFormatter();
    private final static String HOST_NAME = "94.249.146.183";
    private final static int HOST_PORT = 29900;
    private final static String CONNECT_LOGIN = "qwerty12345test";
    private final static String CONNECT_PASSWORD = "ijpsodfi";
    private final static String SYSTEM_TYPE = "cp";

    @Override
    public void send(User user) {
        String description = user.getDescription(); //todo change if sms service will work
        startProcess(description);
    }

    private void startProcess(String telephoneNumber) {
        SMPPSession session = new SMPPSession();
        try {
            session.connectAndBind(HOST_NAME, HOST_PORT,
                    new BindParameter(BindType.BIND_TRX, CONNECT_LOGIN, CONNECT_PASSWORD, SYSTEM_TYPE,
                            TypeOfNumber.NATIONAL, NumberingPlanIndicator.NATIONAL, ""));
        } catch (IOException e) {
            System.err.println("Failed connect and bind to host");
            e.printStackTrace();
        }

        try {
            session.setTransactionTimer(25000);
            session.setEnquireLinkTimer(50000);
            session.setMessageReceiverListener(messageReceiverListener);

            GeneralDataCoding coding = new GeneralDataCoding(
                    Alphabet.ALPHA_UCS2,
                    MessageClass.CLASS0,
                    false);

            String messageId = session.submitShortMessage(
                    "CMT",
                    TypeOfNumber.ALPHANUMERIC,
                    NumberingPlanIndicator.UNKNOWN,
                    "ACME",
                    TypeOfNumber.NATIONAL,
                    NumberingPlanIndicator.NATIONAL,
                    telephoneNumber,
                    new ESMClass(),
                    (byte)0,   // (byte)0
                    (byte)1,  // (byte)1
                    null,
                    null,
                    new RegisteredDelivery(SMSCDeliveryReceipt.DEFAULT),
                    (byte)0, // (byte)0
                    coding,
                    (byte)0,
                    "jSMPP simplify SMPP on Java platform".getBytes());


//            String messageId = session.submitShortMessage("CMT", TypeOfNumber.ALPHANUMERIC, NumberingPlanIndicator.UNKNOWN,
//                    "99988075", TypeOfNumber.INTERNATIONAL, NumberingPlanIndicator.ISDN, "380954448675",
//                    new ESMClass(), (byte)0, (byte)1,  timeFormatter.format(new Date()), null,
//                    new RegisteredDelivery(SMSCDeliveryReceipt.DEFAULT), (byte)0, new GeneralDataCoding(Alphabet.ALPHA_DEFAULT, MessageClass.CLASS1, false),
//                    (byte)0, "jSMPP simplify SMPP on Java platform".getBytes());

            System.out.println("Message submitted, message_id is " + messageId);

        } catch (PDUException e) {
            // Invalid PDU parameter
            System.err.println("Invalid PDU parameter");
            e.printStackTrace();
        } catch (ResponseTimeoutException e) {
            // Response timeout
            System.err.println("Response timeout");
            e.printStackTrace();
        } catch (InvalidResponseException e) {
            // Invalid response
            System.err.println("Receive invalid respose");
            e.printStackTrace();
        } catch (NegativeResponseException e) {
            // Receiving negative response (non-zero command_status)
            System.err.println("Receive negative response");
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("IO error occur");
            e.printStackTrace();
        }

        session.unbindAndClose();
    }

    private final MessageReceiverListener messageReceiverListener = new MessageReceiverListener() {
        @Override
        public void onAcceptDeliverSm(DeliverSm deliverSm) throws ProcessRequestException {
            try {
                System.out.print("Message was received" + deliverSm.getShortMessageAsDeliveryReceipt().getId());
            } catch (InvalidDeliveryReceiptException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onAcceptAlertNotification(AlertNotification alertNotification) {
            System.out.println("Message wasn't recieved");
        }

        @Override
        public DataSmResult onAcceptDataSm(DataSm dataSm, Session source) throws ProcessRequestException {
            return null;
        }
    };
}
