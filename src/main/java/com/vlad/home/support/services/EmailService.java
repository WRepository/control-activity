package com.vlad.home.support.services;

import com.vlad.home.model.User;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;

import javax.mail.internet.MimeMessage;

/**
 * Created by Vlad Z
 */
@Component
public class EmailService implements Service {

    private final static Logger logger = Logger.getLogger(EmailService.class);

    @Autowired
    @Qualifier("mailSender")
    private JavaMailSender mailSender;

    @Override
    public void send(User user) {
        MimeMessagePreparator preparator = new MimeMessagePreparator() {
            @Override
            public void prepare(MimeMessage mimeMessage) throws Exception {
                MimeMessageHelper message = new MimeMessageHelper(mimeMessage, 2, "UTF-8");
                message.setFrom("Vendetta <vladuxa.70@yandex.ua>");
                message.setTo(user.getEmailAdress());
                message.setSubject("For Work.");

                message.setText("Bro you might create you own business logic with reach code", true);
            }
        };
        try {
            mailSender.send(preparator);
        } catch (MailException e) {
            e.printStackTrace();
            logger.error("Error Email wasn't sent to: " + user.getEmailAdress() + e.getMessage());
        }
        logger.info("Email was sent to: " + user.getEmailAdress());
    }
}
