package com.vlad.home.support.services;

import com.vlad.home.model.User;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * Created by Vlad Z
 */
public class EmailJobExecutor implements Job {

    public EmailJobExecutor() {}

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        JobDataMap data = jobExecutionContext.getMergedJobDataMap();
        Service service = (Service) data.get("service");
        User user = (User) data.get("user");

        service.send(user);
    }
}
