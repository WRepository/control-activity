package com.vlad.home.support.services;

import com.vlad.home.model.User;
import org.springframework.stereotype.Component;

/**
 * Created by Vlad Z
 */
@Component
public class MockService implements Service {
    @Override
    public void send(User user) {

    }
}
