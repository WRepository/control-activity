package com.vlad.home.support.services;

import com.vlad.home.model.User;

/**
 * Created by Vlad Z
 */
public interface Service {
    void send(User user);
}
