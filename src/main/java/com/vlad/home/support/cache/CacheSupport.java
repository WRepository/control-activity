package com.vlad.home.support.cache;

/**
 * Created by Vlad.
 *
 */
public class CacheSupport {

    public class Keys{
        private final static String POSTFIX = "-Item";
        public final static String APPLICATION_IMAGE = "Application-Image" + POSTFIX;
    }

    public static String doCompositeKey(String key, Object compose) {
        if (compose instanceof Number) {
            return key.concat(":").concat(String.valueOf(compose));
        } else if (compose instanceof String) {
            return key.concat(":").concat((String) compose);
        } else {
            return null;
        }
    }

}
