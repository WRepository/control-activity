package com.vlad.home.support.models;

import java.util.Date;

/**
 * Created by Vlad Z
 */
public class MessageItem {
    private Long idOwner;
    private String message;
    private Date timeStamp;

    public MessageItem(Long idOwner, String message, Date timeStamp) {
        this.idOwner = idOwner;
        this.message = message;
        this.timeStamp = timeStamp;
    }

    public Long getIdOwner() {
        return idOwner;
    }

    public void setIdOwner(Long idOwner) {
        this.idOwner = idOwner;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }
}
