package com.vlad.home.support.factory;

import com.vlad.home.support.services.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created by Vlad Z
 */
@Component
public class ServiceFactory {

    @Autowired
    @Qualifier("emailService")
    private Service emailService;

    @Autowired
    @Qualifier("smsService")
    private Service smsService;

    @Autowired
    @Qualifier("mockService")
    private Service mockService;

    public Service getServiceByName(String name) {
        if (name.equals("sms")) {
            return smsService;
        } else if (name.equals("email")) {
            return emailService;
        } else {
            return mockService;
        }
    }

}
