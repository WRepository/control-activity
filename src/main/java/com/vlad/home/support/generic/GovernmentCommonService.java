package com.vlad.home.support.generic;

import com.vlad.home.model.BankGovernment;
import com.vlad.home.model.CarProducer;
import com.vlad.home.model.Government;
import com.vlad.home.services.BankGovernmentService;
import com.vlad.home.services.CarGovernmentService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created by Vlad.
 *
 */
@Component
public class GovernmentCommonService {

    private final static Logger logger = Logger.getLogger(GovernmentCommonService.class);

    @Autowired
    private CarGovernmentService carGovernmentService;
    @Autowired
    private BankGovernmentService bankGovernmentService;


    public void encreaseEmployeeCount(Government z, Integer count) {
        logger.info("try increase count of employee to");

        z.setCountOfEmployees(z.getCountOfEmployees() + count);
        if (z instanceof BankGovernment) {
            bankGovernmentService.update((BankGovernment) z);
        } else if (z instanceof CarProducer) {
            carGovernmentService.update((CarProducer) z);
        }

    }


}
