package com.vlad.home.support.exception;

/**
 * Created by Vlad Z
 */
public class ShedulerExistException extends Exception {

    private String message;

    public ShedulerExistException(String message, String key) {
        super(message);
        this.message = "This job already in work: " + key;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
