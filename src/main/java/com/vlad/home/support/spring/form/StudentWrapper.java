package com.vlad.home.support.spring.form;

import com.vlad.home.support.spring.form.source.StudentSource;

/**
 * Created by vlad.
 */
public class StudentWrapper {

    public StudentWrapper() {}

    public StudentWrapper(StudentSource studentSource) {
        this.studentSource = studentSource;
    }

    private Student student;
    private StudentSource studentSource;

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public StudentSource getStudentSource() {
        return studentSource;
    }

}
