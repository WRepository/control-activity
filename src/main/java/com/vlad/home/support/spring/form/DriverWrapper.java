package com.vlad.home.support.spring.form;

import com.vlad.home.model.Driver;

import java.util.List;

/**
 * Created by Vlad.
 *
 */
public class DriverWrapper {

    private Driver driver;
    /*Edit Add*/
    private String type;

    private List<String> busesIdToRemove;


    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getBusesIdToRemove() {
        return busesIdToRemove;
    }

    public void setBusesIdToRemove(List<String> busesIdToRemove) {
        this.busesIdToRemove = busesIdToRemove;
    }
}
