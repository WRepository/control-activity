package com.vlad.home.support.spring.validation;

import com.vlad.home.model.BankGovernment;
import com.vlad.home.support.spring.form.GovernmentWrapper;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.Date;

/**
 * Created by vlad.
 */
public class GovernmentValidator implements Validator{

    private ReloadableResourceBundleMessageSource messageSource;

    public GovernmentValidator() {}

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz == GovernmentWrapper.class;
    }

    @Override
    public void validate(Object target, Errors errors) {
        GovernmentWrapper wrapper = (GovernmentWrapper) target;
        String type = wrapper.getType();

        if (type.equals("BANKS")) {
            BankGovernment bank = wrapper.getBankGovernment();

            String name = bank.getName();
            Integer countOfEmployees = bank.getCountOfEmployees();
            String headMens = bank.getHeadMens();

            if (name.length() < 3 || name.length() > 12) {
                errors.rejectValue("bankGovernment.name", "errors-bankGovernment-userNameLength");
            }
            ValidationUtils.rejectIfEmpty(errors, "bankGovernment.dateOfBase", "errors-bankGovernment-emptyDate");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bankGovernment.countOfEmployees", "errors-bankGovernment-emptyCountOfEmployees");
            if (countOfEmployees != null && countOfEmployees > 100) {
                errors.rejectValue("bankGovernment.countOfEmployees", "errors-bankGovernment-maxCountOfEmployees");
            }
            if (headMens.length() < 3 || headMens.length() > 25) {
                errors.rejectValue("bankGovernment.headMens", "errors-bankGovernment-headMensLength");
            }
        }
    }

}
