package com.vlad.home.support.spring.form;

import com.vlad.home.model.BankGovernment;
import com.vlad.home.model.CarProducer;

import java.util.List;

/**
 * Created by Vlad.
 *
 */
public class GovernmentWrapper {

    private String action;
    private BankGovernment bankGovernment;
    private CarProducer carProducer;
    private String type;

    private List<BankGovernment> bankGovernments;
    private List<CarProducer> carProducers;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public BankGovernment getBankGovernment() {
        return bankGovernment;
    }

    public void setBankGovernment(BankGovernment bankGovernment) {
        this.bankGovernment = bankGovernment;
    }

    public CarProducer getCarProducer() {
        return carProducer;
    }

    public void setCarProducer(CarProducer carProducer) {
        this.carProducer = carProducer;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<BankGovernment> getBankGovernments() {
        return bankGovernments;
    }

    public void setBankGovernments(List<BankGovernment> bankGovernments) {
        this.bankGovernments = bankGovernments;
    }

    public List<CarProducer> getCarProducers() {
        return carProducers;
    }

    public void setCarProducers(List<CarProducer> carProducers) {
        this.carProducers = carProducers;
    }

}
