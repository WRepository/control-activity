package com.vlad.home.support.spring.form.source;

import com.vlad.home.model.BankGovernment;
import com.vlad.home.support.spring.form.Hostel;

import java.util.*;

/**
 * Created by vlad.
 */
public class StudentSource {

    public StudentSource() {
        selectedCourses = new HashMap<>();

        selectedCourses.put("Mathematics", Boolean.FALSE);
        selectedCourses.put("History", Boolean.FALSE);
        selectedCourses.put("Geography", Boolean.FALSE);

        weekCash = new ArrayList<>();
        weekCash.add("100$");
        weekCash.add("40$");

        availableHostels = new ArrayList<>();
        Hostel first = new Hostel();
        first.setId(10L);
        first.setName("Пятерка");
        first.setAddress("Axsarova str. house:18");
        availableHostels.add(first);

        Hostel second = new Hostel();
        second.setId(19L);
        second.setName("19");
        second.setAddress("Darvina str. number house:19");
        availableHostels.add(second);

        Hostel third = new Hostel();
        third.setId(20L);
        third.setName("20");
        third.setAddress("Darvina str. number house:20");
        availableHostels.add(third);
    }

    private final Map<String, Boolean> selectedCourses;
    private final List<String> weekCash;
    private final List<Hostel> availableHostels;

    public Map<String, Boolean> getSelectedCourses() {
        return selectedCourses;
    }

    public List<String> getWeekCash() {
        return weekCash;
    }

    public List<Hostel> getAvailableHostels() {
        return availableHostels;
    }

}
