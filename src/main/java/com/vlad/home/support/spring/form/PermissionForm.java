package com.vlad.home.support.spring.form;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by vlad.
 */
public class PermissionForm {

    private Map<String, Map<String, Boolean>> usersPermissionsBridge;
//    private Map<String, Boolean> usersPermissionsBridge;
    private String lastSelected;

    public Map<String, Map<String, Boolean>> getUsersPermissionsBridge() {
        return usersPermissionsBridge;
    }

    public void setUsersPermissionsBridge(Map<String, Map<String, Boolean>> usersPermissionsBridge) {
        this.usersPermissionsBridge = usersPermissionsBridge;
    }


//    public Map<String, Boolean> getUsersPermissionsBridge() {
//        return usersPermissionsBridge;
//    }

//    public void setUsersPermissionsBridge(Map<String, Boolean> usersPermissionsBridge) {
//        this.usersPermissionsBridge = usersPermissionsBridge;
//    }

    public String getLastSelected() {
        return lastSelected;
    }

    public void setLastSelected(String lastSelected) {
        this.lastSelected = lastSelected;
    }
}
