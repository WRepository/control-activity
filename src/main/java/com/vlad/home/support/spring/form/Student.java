package com.vlad.home.support.spring.form;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by vlad.
 */
public class Student {

    private Long testId;
    private String name;
    private Integer age;
    private String weekCash;
    private Double salary;

    private Hostel hostel;
    private Set<String> learningSubjects;
    private List<String> friends;
    private Map<String, Boolean> selectedCourses;
    private Boolean activeCheckBox;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getWeekCash() {
        return weekCash;
    }

    public void setWeekCash(String weekCash) {
        this.weekCash = weekCash;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public Hostel getHostel() {
        return hostel;
    }

    public void setHostel(Hostel hostel) {
        this.hostel = hostel;
    }

    public Set<String> getLearningSubjects() {
        return learningSubjects;
    }

    public void setLearningSubjects(Set<String> learningSubjects) {
        this.learningSubjects = learningSubjects;
    }

    public List<String> getFriends() {
        return friends;
    }

    public void setFriends(List<String> friends) {
        this.friends = friends;
    }

    public Map<String, Boolean> getSelectedCourses() {
        return selectedCourses;
    }

    public void setSelectedCourses(Map<String, Boolean> selectedCourses) {
        this.selectedCourses = selectedCourses;
    }

    public Long getTestId() {
        return testId;
    }

    public void setTestId(Long testId) {
        this.testId = testId;
    }

    public Boolean getActiveCheckBox() {
        return activeCheckBox;
    }

    public void setActiveCheckBox(Boolean activeCheckBox) {
        this.activeCheckBox = activeCheckBox;
    }
}
