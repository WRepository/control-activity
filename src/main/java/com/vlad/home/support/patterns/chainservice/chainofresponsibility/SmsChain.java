package com.vlad.home.support.patterns.chainservice.chainofresponsibility;

import com.vlad.home.application.SpringApplicationContext;
import com.vlad.home.model.User;
import com.vlad.home.services.UserService;
import com.vlad.home.support.services.Service;

/**
 * Created by Vlad Z
 */
public class SmsChain extends BaseChain {
    public SmsChain(Service service) {
        super(service);
    }

    @Override
    public void startAction(Long userId) {

        UserService userService = (UserService) SpringApplicationContext.getBean("userServiceImpl");

        User user = userService.getEntityById(userId);

        service.send(user);

        if (candidate != null)
            candidate.startAction(userId);
    }
}
