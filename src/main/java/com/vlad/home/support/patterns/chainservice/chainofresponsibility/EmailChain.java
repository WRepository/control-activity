package com.vlad.home.support.patterns.chainservice.chainofresponsibility;

import com.vlad.home.application.SpringApplicationContext;
import com.vlad.home.model.User;
import com.vlad.home.services.UserService;
import com.vlad.home.support.services.Service;

/**
 * Created by Vlad Z
 */
public class EmailChain extends BaseChain {

    public EmailChain(Service service) {
        super(service);
    }

    @Override
    public void startAction(Long userId) {

        UserService userService = (UserService) SpringApplicationContext.getBean(UserService.class);

        User userByEmail = userService.getEntityById(userId);

        service.send(userByEmail);

        if (candidate != null)
            candidate.startAction(userId);

    }
}
