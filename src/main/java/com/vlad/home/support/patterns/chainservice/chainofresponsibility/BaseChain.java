package com.vlad.home.support.patterns.chainservice.chainofresponsibility;

import com.vlad.home.support.services.Service;

/**
 * Created by Vlad Z
 */
public abstract class BaseChain {

    protected Service service;
    protected BaseChain candidate;

    public BaseChain(Service service) {
        this.service = service;
    }

    public abstract void startAction(Long userId);

    public void setCandidate(BaseChain candidate) {
        this.candidate = candidate;
    }
}
