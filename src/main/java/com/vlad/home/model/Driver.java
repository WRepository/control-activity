package com.vlad.home.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Vlad.
 *
 */
@Entity
@Table(name = "drivers")
public class Driver extends BaseModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "driver_id")
    private Long id;

    @Column(name = "name")
    private String name;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.EAGER)
    @JoinTable(name = "drivers_buses",
            joinColumns = @JoinColumn(name = "driver_id_inner", referencedColumnName = "driver_id"),
            inverseJoinColumns = @JoinColumn(name = "bus_id_inner", referencedColumnName = "bus_id"))
    private List<Bus> buses;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Bus> getBuses() {
        return buses;
    }

    public void setBuses(List<Bus> buses) {
        this.buses = buses;
    }
}
