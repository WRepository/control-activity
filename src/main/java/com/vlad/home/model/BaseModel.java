package com.vlad.home.model;

import java.io.Serializable;

/**
 * Created by Vlad.
 *
 */
public class BaseModel implements Serializable {

    private final static long serialVersionUID = 42L;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
}
