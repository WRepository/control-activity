package com.vlad.home.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Vlad.
 *
 */
@Entity
@Table(name = "bank_government")
public class BankGovernment extends Government {

    @Column(name = "based_date")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private Date dateOfBase;

    @Column(name = "head_mens")
    private String headMens;

    public Date getDateOfBase() {
        return dateOfBase;
    }

    public void setDateOfBase(Date dateOfBase) {
        this.dateOfBase = dateOfBase;
    }

    public String getHeadMens() {
        return headMens;
    }

    public void setHeadMens(String headMens) {
        this.headMens = headMens;
    }

}
