package com.vlad.home.model;

import com.vlad.home.services.impl.CurrentTaskServiceImpl;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Vlad.
 *
 */
@Entity
@Table(name = "tasks")
public class CurrentTask extends BaseModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "task_id")
    private Long id;

    @Column(name = "text", nullable = false)
    private String text;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "state", nullable = false, length = 15)
    private CurrentTaskServiceImpl.TaskState state;

    @Column(name = "created_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @Column(name = "finish_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finishDate;

    @Column(name = "rejected_description")
    private String rejectedDescription;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public CurrentTaskServiceImpl.TaskState getState() {
        return state;
    }

    public void setState(CurrentTaskServiceImpl.TaskState state) {
        this.state = state;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public String getRejectedDescription() {
        return rejectedDescription;
    }

    public void setRejectedDescription(String rejectedDescription) {
        this.rejectedDescription = rejectedDescription;
    }
}
