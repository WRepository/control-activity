package com.vlad.home.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Vlad.
 *
 */
@Entity
@Table(name = "government_history")
public class GovernmentHistoryParameter extends BaseModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "type")
    private String selectedTypeHistory;

    @Column(name = "selected_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date selected;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSelectedTypeHistory() {
        return selectedTypeHistory;
    }

    public void setSelectedTypeHistory(String selectedTypeHistory) {
        this.selectedTypeHistory = selectedTypeHistory;
    }

    public Date getSelected() {
        return selected;
    }

    public void setSelected(Date selected) {
        this.selected = selected;
    }
}
