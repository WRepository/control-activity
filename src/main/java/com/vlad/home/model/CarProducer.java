package com.vlad.home.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Vlad.
 *
 */
@Entity
@Table(name = "car_government")
public class CarProducer extends Government{

    @Column(name = "opened_filials")
    private String rawOpenedFilials;

    @Column(name = "produced_models")
    private String rawProducedModels;

    public String getRawOpenedFilials() {
        return rawOpenedFilials;
    }

    public void setRawOpenedFilials(String rawOpenedFilials) {
        this.rawOpenedFilials = rawOpenedFilials;
    }

    public String getRawProducedModels() {
        return rawProducedModels;
    }

    public void setRawProducedModels(String rawProducedModels) {
        this.rawProducedModels = rawProducedModels;
    }
}
