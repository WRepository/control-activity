package com.vlad.home.model;

import javax.persistence.*;

/**
 * Created by Vlad.
 *
 */
@MappedSuperclass
public class Government extends BaseModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "count_of_employees")
    private Integer countOfEmployees;

    @Column(name = "type")
    private String type;

    @Column(name = "name")
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCountOfEmployees() {
        return countOfEmployees;
    }

    public void setCountOfEmployees(Integer countOfEmployees) {
        this.countOfEmployees = countOfEmployees;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
