package com.vlad.home.dto;

/**
 * Created by Vlad Z
 */
public class UserInfoDTO {

    private String fullName;
    private String email;
    private String dateBirthday;
//    private List<HImage> hImages;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDateBirthday() {
        return dateBirthday;
    }

    public void setDateBirthday(String dateBirthday) {
        this.dateBirthday = dateBirthday;
    }

//    public List<HImage> gethImages() {
//        return hImages;
//    }
//
//    public void sethImages(List<HImage> hImages) {
//        this.hImages = hImages;
//    }
}
