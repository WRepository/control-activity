package com.vlad.home.services;

import com.vlad.home.model.BaseModel;

import java.io.Serializable;

/**
 * Created by Vlad.
 *
 */
public interface BaseService<T extends BaseModel, PK extends Serializable> {
    void save(T entity);

    void saveOrUpdate(T entity);

    T getEntityById(PK id);

    void update(T entity);

    void delete(T entity);

    void persist(T entity);

    T merge(T entity);
}
