package com.vlad.home.services;

import com.vlad.home.dto.UserInfoDTO;
import com.vlad.home.model.User;

public interface UserService extends BaseService<User, Long> {

    User getUserByName(String name, boolean sortedImage);

    User getUserByEmail(String email);

    boolean emailExist(String email);

    UserInfoDTO getUserInfo(String email);
}
