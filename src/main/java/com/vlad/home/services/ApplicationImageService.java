package com.vlad.home.services;

import com.vlad.home.model.ApplicationImage;
import org.apache.commons.fileupload.FileUploadException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Vlad.
 *
 */
public interface ApplicationImageService {

    void removeAllJDBCImages();

    void processSaveImage(HttpServletRequest request, String pageView) throws FileUploadException, IOException;

    void processShowImage(HttpServletRequest request, HttpServletResponse response) throws IOException;

    List<ApplicationImage> getImagesByCategory(String category);
}
