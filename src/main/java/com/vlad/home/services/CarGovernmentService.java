package com.vlad.home.services;

import com.vlad.home.model.CarProducer;

import java.util.List;

/**
 * Created by Vlad.
 *
 */
public interface CarGovernmentService extends BaseService<CarProducer, Long> {
    List<CarProducer> getAll();
}
