package com.vlad.home.services;

import com.vlad.home.model.Driver;

/**
 * Created by Vlad.
 *
 */
public interface DriverBusService {

    Object getAllObjects(Class aClass);

    public void testMergeNadUpdate(Driver entity);
}
