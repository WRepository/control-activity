package com.vlad.home.services.impl;

import com.vlad.home.dao.ApplicationImageDAO;
import com.vlad.home.dao.UserDao;
import com.vlad.home.model.ApplicationImage;
import com.vlad.home.model.User;
import com.vlad.home.services.ApplicationImageService;
import com.vlad.home.services.UserService;
import com.vlad.home.support.cache.CacheSupport;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.Cache;
import org.springframework.cache.ehcache.EhCacheCache;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Vlad.
 *
 */
@Service
@Transactional
public class ApplicationImageServiceImpl implements ApplicationImageService {

    private final static Logger LOGGER = Logger.getLogger(ApplicationImageServiceImpl.class);

    @Autowired
    private ApplicationImageDAO applicationImageDAO;

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserService userService;

    @Value("#{cacheManager.getCache('all-photo')}")
    private EhCacheCache cache;

    @Override
    public void removeAllJDBCImages() {
        applicationImageDAO.deleteJDBCPage();
    }

    @Override
    public void processSaveImage(HttpServletRequest request, String pageView) throws FileUploadException, IOException {

        FileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload servletFileUpload = new ServletFileUpload(factory);

        List<FileItem> fileItems = servletFileUpload.parseRequest(request);

        Long userId = null;
        String fileName = null;
        String format = null;
        byte[] blob = null;
        String category = null;

        Iterator<FileItem> iter = fileItems.iterator();
        while (iter.hasNext()) {
            FileItem item = iter.next();
            if (item.isFormField()) {
                if (item.getFieldName().equals("userId")) {
                    userId = Long.valueOf(item.getString());
                }
                if (item.getFieldName().equals("categoryKeyId")) {
                    category = item.getString();
                }
            } else {
                if (item.getFieldName().equals("file")) {
                    fileName = item.getName();
                    format = item.getContentType();
                    blob = item.get();
                }
            }
        }
        if (blob != null && blob.length > 0) {
            ApplicationImage applicationImage = new ApplicationImage();

            if (userId != null) {
                User user = userDao.getEntityById(userId);
                applicationImage.setUser(user);
            }
            applicationImage.setFormat(format);
            applicationImage.setName(fileName);
            applicationImage.setBytes(blob);
            applicationImage.setDate(new Date());
            applicationImage.setPageView(pageView);
            applicationImage.setCategory(category);

            applicationImageDAO.saveOrUpdate(applicationImage);
        }

    }

    @Override
    public void processShowImage(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String imageId = request.getParameter("imageId");

        if (imageId != null && !imageId.isEmpty()) {

            Long userIdPrepared = Long.valueOf(imageId);

            final ApplicationImage image;

            String compositeKey = CacheSupport.doCompositeKey(CacheSupport.Keys.APPLICATION_IMAGE, imageId);
            Cache.ValueWrapper wrapper = cache.get(compositeKey);
            if (wrapper == null) {
                image = applicationImageDAO.getEntityById(userIdPrepared);
                LOGGER.info(compositeKey + " COME FROM DAO");
                cache.put(compositeKey, image);
            } else {
                image = (ApplicationImage) wrapper.get();
                LOGGER.info(compositeKey + " COME FROM CACHE");
            }

            if (image != null) {

                response.setHeader("Content-Disposition", "inline; filename=\"" + image.getName() + "\"");
                response.setContentType(image.getFormat());

                ServletOutputStream outputStream = response.getOutputStream();
                outputStream.write(image.getBytes());
                outputStream.flush();

            }
        }
    }

    @Override
    public List<ApplicationImage> getImagesByCategory(String category) {
        return applicationImageDAO.getImagesByCategory(category);
    }
}
