package com.vlad.home.services.impl;

import com.vlad.home.dao.BankGovernmentDao;
import com.vlad.home.model.BankGovernment;
import com.vlad.home.services.BankGovernmentService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Vlad.
 *
 */
@Service
@Transactional
public class BankGovernmentServiceImpl extends BaseServiceImpl<BankGovernment, Long> implements BankGovernmentService{

    private final static Logger logger = Logger.getLogger(BankGovernmentService.class);

    @Autowired
    private BankGovernmentDao dao;

    public BankGovernmentServiceImpl() {
        super(BankGovernment.class);
    }

    @Override
    public List<BankGovernment> getAll() {
        logger.info(PREFIX_ALL + "BankGovernment" + SUFFIX_ALL);
        return dao.getAll();
    }
}
