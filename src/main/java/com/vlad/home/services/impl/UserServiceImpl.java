package com.vlad.home.services.impl;

import com.vlad.home.dao.UserDao;
import com.vlad.home.dto.UserInfoDTO;
import com.vlad.home.model.User;
import com.vlad.home.services.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl extends BaseServiceImpl<User, Long> implements UserService {

    private final static Logger logger = Logger.getLogger(UserServiceImpl.class);//TODO use logger on prod

    public UserServiceImpl() {
        super(User.class);
    }

    @Autowired
    private UserDao userDao;

    @Override
    public User getUserByName(String name, boolean sortedImage) {

        if (name != null && !name.isEmpty()) {

            User lUser = userDao.getUserByName(name);

            return lUser;
        }
        return null;
    }

    @Override
    public User getUserByEmail(String email) {
        String emailPrepared = email.trim();

        User user = userDao.getUserByEmail(emailPrepared);

        return user;
    }

    @Override
    public boolean emailExist(String email) {
        String emailPrepared = email.trim();

        User user = userDao.getUserByEmail(emailPrepared);
        boolean userExist = user != null;
        return userExist;
    }

    @Override
    public UserInfoDTO getUserInfo(String email) {
        String emailPrepared = email.trim();

        User user = getUserByEmail(emailPrepared);

        UserInfoDTO infoDTO = new UserInfoDTO();
        infoDTO.setFullName(String.format("%s %s.", user.getUserName(), user.getSurName()));
        infoDTO.setDateBirthday(String.format("%1$te.%1$tm.%1$tY", user.getDateBirthday()));
        infoDTO.setEmail(user.getEmailAdress());
//        infoDTO.sethImages(user.getUserPhotos());

        return infoDTO;
    }
}
