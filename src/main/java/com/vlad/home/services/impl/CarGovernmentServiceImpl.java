package com.vlad.home.services.impl;

import com.vlad.home.dao.CarGovernmentDao;
import com.vlad.home.model.CarProducer;
import com.vlad.home.services.CarGovernmentService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Vlad.
 *
 */
@Service
@Transactional
public class CarGovernmentServiceImpl extends BaseServiceImpl<CarProducer, Long> implements CarGovernmentService {

    private final static Logger logger = Logger.getLogger(CarGovernmentServiceImpl.class);

    @Autowired
    private CarGovernmentDao dao;

    public CarGovernmentServiceImpl() {
        super(CarProducer.class);
    }

    @Override
    public List<CarProducer> getAll() {
        logger.info(CarGovernmentServiceImpl.PREFIX_ALL + "CarProducer" + CarGovernmentServiceImpl.SUFFIX_ALL);
        return dao.getAll();
    }
}
