package com.vlad.home.services.impl;

import com.vlad.home.dao.CurrentTaskDao;
import com.vlad.home.model.CurrentTask;
import com.vlad.home.services.CurrentTaskService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by Vlad.
 *
 */
@Service
@Transactional
public class CurrentTaskServiceImpl extends BaseServiceImpl<CurrentTask, Long> implements CurrentTaskService {

    @Autowired
    private CurrentTaskDao taskDao;

    private final Logger logger = Logger.getLogger(CurrentTaskServiceImpl.class);

    public CurrentTaskServiceImpl() {
        super(CurrentTask.class);
    }

    public enum TaskState {

        NEW("NEW", -1),
        MEDIUM("MEDIUM", 0),
        COMPLETED("COMPLETED", 1),
        REJECTED("REJECTED", -99);

        private String state;
        private Integer stateRepresentation;

        TaskState(String state, Integer stateRepresentation) {
            this.state = state;
            this.stateRepresentation = stateRepresentation;
        }

        public String getState() {
            return state;
        }
    }

    @Override
    public List<CurrentTask> getAll() {
        logger.info(PREFIX_ALL + "CurrentTask" + SUFFIX_ALL);

        List<CurrentTask> allTasks = taskDao.getAll();

        return allTasks;
    }

    @Override
    public void addNewTask(String textBody) {
        logger.info("Trying add new task");

        CurrentTask currentTask = new CurrentTask();
        currentTask.setCreatedDate(new Date());
        currentTask.setText(textBody);
        currentTask.setState(TaskState.NEW);

        super.persist(currentTask);
    }

    @Override
    public void taskWasMade(Long candidate) {
        logger.info("Trying add MEDIUM state for task with id: " + candidate);

        CurrentTask task = taskDao.getEntityById(candidate);

        if (task.getState().equals(TaskState.NEW)) {
            task.setState(TaskState.MEDIUM);
            task.setFinishDate(new Date());
            taskDao.update(task);
        }
    }

    @Override
    public void markAsComplete(Long candidate) {
        logger.info("Trying COMPLETE task with id: " + candidate);

        CurrentTask task = taskDao.getEntityById(candidate);

        if (task.getState().equals(TaskState.MEDIUM)) {
            task.setState(TaskState.COMPLETED);
            taskDao.update(task);
        }
    }

    public List<CurrentTask> getTasksByStatus(List<CurrentTask> tasks, CurrentTaskServiceImpl.TaskState... taskStates) {
        List<CurrentTaskServiceImpl.TaskState> taskStateList = Arrays.asList(taskStates);

        List<CurrentTask> completedTasks = new ArrayList<>();
        for (CurrentTask task : tasks) {
            if (taskStateList.contains(task.getState())) {
                completedTasks.add(task);
            }
        }
        return completedTasks;
    }


}
