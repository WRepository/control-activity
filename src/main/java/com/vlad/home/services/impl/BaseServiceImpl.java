package com.vlad.home.services.impl;

import com.vlad.home.dao.BaseDAO;
import com.vlad.home.model.BaseModel;
import com.vlad.home.model.CarProducer;
import com.vlad.home.services.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * Created by Vlad.
 *
 */
@Service
@Transactional
public class BaseServiceImpl<T extends BaseModel, PK extends Serializable> extends ServiceSupport implements BaseService<T, PK> {

    @Autowired
    @Qualifier("BaseDaoImpl")
    private BaseDAO<T, PK> baseDao;

    private Class<T> persistenceClass;

    public BaseServiceImpl(Class<T> persistenceClass) {
        this.persistenceClass = persistenceClass;
    }

    public BaseServiceImpl() {}

    @Override
    public void save(T entity) {
        baseDao.save(entity);
    }

    @Override
    public void saveOrUpdate(T entity) {
        baseDao.saveOrUpdate(entity);
    }

    @Override
    public T getEntityById(PK id) {
        return baseDao.getEntityById(id, persistenceClass);
    }

    @Override
    public void update(T entity) {
        baseDao.update(entity);
    }

    @Override
    public void delete(T entity) {
        baseDao.delete(entity);
    }

    @Override
    public void persist(T entity) {
        baseDao.persist(entity);
    }

    @Override
    public T merge(T entity) {
        return baseDao.merge(entity);
    }

}
