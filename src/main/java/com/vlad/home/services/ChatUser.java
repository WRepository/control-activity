package com.vlad.home.services;

import com.vlad.home.support.models.MessageItem;

/**
 * Created by Vlad Z
 */
public interface ChatUser {
    void sendMessage(MessageItem messageItem);
}
