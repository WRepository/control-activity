package com.vlad.home.services;

import com.vlad.home.model.GovernmentHistoryParameter;

/**
 * Created by Vlad.
 *
 */
public interface GovernmentHistoryService extends BaseService<GovernmentHistoryParameter, Long>{
    GovernmentHistoryParameter getLastParameter();
}
