package com.vlad.home.services.additional.impl;

import com.vlad.home.model.User;
import com.vlad.home.services.ChatUser;
import com.vlad.home.support.models.MessageItem;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;

/**
 * Created by Vlad Z
 */
public class SimpleChatUserImpl implements ChatUser {

    private final HttpServletResponse httpServletResponse;
    private final User responseOwner;

    public SimpleChatUserImpl(HttpServletResponse httpServletResponse, User responseOwner) {
        this.httpServletResponse = httpServletResponse;
        this.responseOwner = responseOwner;
    }

    @Override
    public void sendMessage(MessageItem messageItem) {

        PrintWriter writer = null;
        try {
            writer = httpServletResponse.getWriter();
        } catch (IOException e) {
            e.printStackTrace();
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        String dateAsString = dateFormat.format(messageItem.getTimeStamp());



        StringBuilder sb = new StringBuilder();
        sb
                .append("{")
                .append("\"message\":\"")
                .append(messageItem.getMessage())
                .append("\"")
                .append(",")
                .append("\"timestamp\":")
                .append("\"")
                .append(dateAsString)
                .append("\"")
                .append("}");

         writer.write(sb.toString());
    }

}
