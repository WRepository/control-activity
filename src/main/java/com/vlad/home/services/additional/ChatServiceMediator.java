package com.vlad.home.services.additional;

import com.vlad.home.model.User;
import com.vlad.home.services.ChatUser;
import com.vlad.home.support.models.MessageItem;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by Vlad Z
 */
@Service
public class ChatServiceMediator {

    private final Object lock = new Object();
    private final Map<Long, ChatUser> usersInChat = new HashMap<>();
    private final Set<MessageItem> messageHistoryCommon = new LinkedHashSet<>();

    public void registerUser(User currUser, ChatUser type) {
        usersInChat.put(currUser.getId(), type);
    }

    public void unregisterUser(User user) {
        synchronized (lock) {
            usersInChat.remove(user.getId());
        }
    }

    public boolean isRegistered(User user) {
        return usersInChat.containsKey(user.getId());
    }

    public void addChatMessage(User from, String message) {
        boolean b = usersInChat.containsKey(from.getId());
        if (!b)
            return;

        MessageItem item = new MessageItem(from.getId(), message, new Date());
        messageHistoryCommon.add(item);

        synchronized (lock) {
            for (ChatUser chatUser : usersInChat.values()) {
                chatUser.sendMessage(item);
            }
        }
    }



}
