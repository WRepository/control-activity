package com.vlad.home.services;

import com.vlad.home.model.BankGovernment;

import java.util.List;

/**
 * Created by Vlad.
 *
 */
public interface BankGovernmentService extends BaseService<BankGovernment, Long> {
    List<BankGovernment> getAll();
}
