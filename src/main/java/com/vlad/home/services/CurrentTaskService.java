package com.vlad.home.services;

import com.vlad.home.model.CurrentTask;
import com.vlad.home.services.impl.CurrentTaskServiceImpl;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * Created by Vlad.
 *
 */
public interface CurrentTaskService extends BaseService<CurrentTask, Long> {
    List<CurrentTask> getAll();

    void addNewTask(String textBody);

    void markAsComplete(Long candidate);

    void taskWasMade(Long candidate);

    List<CurrentTask> getTasksByStatus(List<CurrentTask> tasks, CurrentTaskServiceImpl.TaskState... taskStates);

}
