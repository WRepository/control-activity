package com.vlad.home.application;

import com.vlad.home.model.User;
import com.vlad.home.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vlad.
 */
@Component
public class CustomUserDetailService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        List<GrantedAuthority> authorityList = new ArrayList<>();

        User user = userService.getUserByEmail(s);

        if (user == null) {
            throw new UsernameNotFoundException("User doesn't exist");
        }

        authorityList.add(new SimpleGrantedAuthority("administrator"));

        UserDetails springUser = new org.springframework.security.core.userdetails.User
                (user.getEmailAdress(), user.getPassword(), authorityList);

        return springUser;
    }
}
