package com.vlad.home.application;

import org.springframework.stereotype.Component;

import javax.servlet.ServletContextEvent;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Vlad.
 *
 */
@Component
public class ApplicationContextListener implements javax.servlet.ServletContextListener {


    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {

        Map<String, String> headers = new LinkedHashMap<>();
        headers.put("renderTask", "My Tasks");
        headers.put("EJB", "My Photo");
        headers.put("springFormConvert", "Spring Form Complex");
        headers.put("services", "Services Types");
        headers.put("chat", "Chat Example");
        headers.put("restController", "Rest Controller");

        servletContextEvent.getServletContext().setAttribute("applicationHeaders", headers);

    }


    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {}

}
