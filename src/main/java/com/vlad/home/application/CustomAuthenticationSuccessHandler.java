package com.vlad.home.application;

import com.vlad.home.model.User;
import com.vlad.home.services.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.UrlUtils;
import org.springframework.util.Assert;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Vlad Z
 */
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private final String forwardUrl;

    /**
     * @param forwardUrl
     */
    public CustomAuthenticationSuccessHandler(String forwardUrl) {
        Assert.isTrue(UrlUtils.isValidRedirectUrl(forwardUrl), "'"
                + forwardUrl + "' is not a valid forward URL");
        this.forwardUrl = forwardUrl;
    }

    /**
     * Called when a user has been successfully authenticated.
     *
     * @param request        the request which caused the successful authentication
     * @param response       the response
     * @param authentication the <tt>Authentication</tt> object which was created during
     */
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        UserDetails principal = (UserDetails) authentication.getPrincipal();
        UserService userService = (UserService) SpringApplicationContext.getBean(UserService.class);
        User user = userService.getUserByEmail(principal.getUsername());
        request.getSession().setAttribute("loggedUser", user);

        request.getRequestDispatcher(forwardUrl).forward(request, response);
    }
}
