package com.vlad.home.controllers.restscontroller;

import com.vlad.home.controllers.BaseController;
import com.vlad.home.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Vlad Z
 */
@Controller
public class RestController extends BaseController {

    private final String GOTOV4ENKO = "restservice/restservice";

    @RequestMapping("/restController")
    protected ModelAndView render(HttpServletRequest request, HttpServletResponse response) {
        return new ModelAndView(GOTOV4ENKO);
    }

    @ResponseBody
    @RequestMapping("/restCall")
    public User restCall(HttpServletRequest request, HttpServletResponse response) {
        User loggedUser = getLoggedUserFromContext();
        return loggedUser;
    }

}
