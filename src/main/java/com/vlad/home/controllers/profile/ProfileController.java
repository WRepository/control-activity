package com.vlad.home.controllers.profile;

import com.vlad.home.controllers.BaseController;
import com.vlad.home.dto.UserInfoDTO;
import com.vlad.home.services.UserService;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by vlad.
 */
@Controller
@PreAuthorize("isAuthenticated()")
public class ProfileController extends BaseController {

    private final static Logger logger = Logger.getLogger(ProfileController.class);//TODO use logger on prod

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public ModelAndView render(HttpServletRequest request, HttpServletResponse response) {

        ModelAndView modelAndView = new ModelAndView("cabinet/profile");

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String email = authentication.getName();    //using email to login instead username

        UserInfoDTO userInfo = userService.getUserInfo(email);
        modelAndView.addObject("privateData", userInfo);

        return modelAndView;
    }

    @RequestMapping(value = "/showLawImage")
    public void showOneImage(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //delegate
//        imageService.processShowImage(request, response);
    }

    @RequestMapping(value = "/saveAllImages", method = RequestMethod.POST)
    public String saveAllImages(HttpServletRequest request, HttpServletResponse response) throws IOException, FileUploadException {
//        imageService.saveAllImages(request);
        return "redirect:/home";
    }

}