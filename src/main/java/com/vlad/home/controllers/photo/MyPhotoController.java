package com.vlad.home.controllers.photo;

import com.vlad.home.controllers.BaseController;
import com.vlad.home.model.User;
import com.vlad.home.services.ApplicationImageService;
import com.vlad.home.services.UserService;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@PreAuthorize("isAuthenticated()")
public class MyPhotoController extends BaseController{

    private final Logger logger = Logger.getLogger(MyPhotoController.class);
    private final String REDIRECT_TO_MAIN = "redirect:/EJB";

    @Autowired
    private UserService userService;

    @Autowired
    private ApplicationImageService applicationImageService;

    @RequestMapping(value = "/EJB")
    public ModelAndView render(HttpServletRequest request, HttpServletResponse response) {
        logger.info("Called method: render()");
        ModelAndView modelAndView = new ModelAndView("photos/view");

        User loggedUser = (User) request.getSession().getAttribute("loggedUser");
        User userWithFullImages = userService.getEntityById(loggedUser.getId());
        modelAndView.addObject("user", userWithFullImages);

        return modelAndView;
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = "UserImage")
    @RequestMapping(value = "/addImage")
    public String addImage(HttpServletRequest request, HttpServletResponse response) throws FileUploadException, IOException {

        logger.info("Called method: addImage()");

        applicationImageService.processSaveImage(request, "OR-page");

        return REDIRECT_TO_MAIN;
    }

    @RequestMapping(value = "/showImage")
    public void showImage(HttpServletRequest request, HttpServletResponse response) throws IOException {

        applicationImageService.processShowImage(request, response);
    }


}

