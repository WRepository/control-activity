package com.vlad.home.controllers;

import com.vlad.home.model.User;
import com.vlad.home.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by vlad.
 */
public abstract class BaseController {

    @Autowired
    protected UserService userService;

    private final static String PREFIX = "redirect:";
    protected final static String REDIRECT_TO_LOGIN = PREFIX + "/login";


    protected abstract ModelAndView render(HttpServletRequest request, HttpServletResponse response);

    protected User getLoggedUserFromContext() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        UserDetails principal = (UserDetails) securityContext.getAuthentication().getPrincipal();

        String email = principal.getUsername();
        User userByEmail = userService.getUserByEmail(email);
        return userByEmail;
    }
}

