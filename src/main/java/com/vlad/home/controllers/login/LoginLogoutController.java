package com.vlad.home.controllers.login;

import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by vlad.
 */
@Controller
public class LoginLogoutController {

    private final static Logger logger = Logger.getLogger(LoginLogoutController.class);

    @RequestMapping(value = "/login")
    public ModelAndView login(@RequestParam(value = "failed", required = false)boolean failed){
        return new ModelAndView("login", "failed", failed);
    }

    @RequestMapping(value = "/logout")
    public ModelAndView logout(HttpServletRequest request, HttpServletResponse response){

        ModelAndView modelAndView = new ModelAndView("login");

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null) {
            SecurityContextLogoutHandler securityContextLogoutHandler = new SecurityContextLogoutHandler();
            securityContextLogoutHandler.logout(request, response, authentication);
        }

        return modelAndView;
    }
}
