package com.vlad.home.controllers.register;

import com.vlad.home.controllers.BaseController;
import com.vlad.home.model.User;
import com.vlad.home.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Vlad.
 */

@Controller
public class RegisterController extends BaseController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public ModelAndView render(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView view = new ModelAndView("register/register-form");
        view.addObject("newUser", new User());

        return view;
    }

    @RequestMapping(value = "/checkExistEmail", method = RequestMethod.POST)
    public void checkMail(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String emailAddress = request.getParameter("emailAdress");

        boolean emailExist = userService.emailExist(emailAddress);

        StringBuilder sb = new StringBuilder();
        sb
                .append("{\"valid\":")
                .append("\"")
                .append(!emailExist)
                .append("\"")
                .append("}");


        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();
        writer.write(sb.toString());
    }

    @RequestMapping(value = "/registerUser", method = RequestMethod.POST)
    public String registerProcess(@ModelAttribute("newUser") User user){
        //server validation here

        userService.persist(user);

        return REDIRECT_TO_LOGIN;
    }

    @InitBinder("newUser")
    protected void bind(WebDataBinder binder){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        binder.registerCustomEditor(Date.class, "dateBirthday", new CustomDateEditor(dateFormat, true));
    }



}
