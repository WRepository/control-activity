package com.vlad.home.controllers.chat;

import com.vlad.home.controllers.BaseController;
import com.vlad.home.model.User;
import com.vlad.home.services.UserService;
import com.vlad.home.services.additional.ChatServiceMediator;
import com.vlad.home.services.additional.impl.SimpleChatUserImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Vlad Z
 */
@Controller
@PreAuthorize("isAuthenticated()")
public class ChatController extends BaseController {

    @Autowired
    private UserService userService;

    @Autowired
    private ChatServiceMediator serviceMediator;

    @RequestMapping(value = "/chat")
    protected ModelAndView render(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView view = new ModelAndView("chat/mainchatview");
        return view;
    }

    @RequestMapping(value = "/addMessage", method = RequestMethod.POST)
    public void addMessage(HttpServletRequest request, HttpServletResponse response) {
        String messagePar = request.getParameter("message");

        User loggedUser = (User) request.getSession().getAttribute("loggedUser");

        serviceMediator.addChatMessage(loggedUser, messagePar);
    }

    @RequestMapping(value = "/startChat", method = RequestMethod.GET)
    public void startUserChat(HttpServletRequest request, HttpServletResponse response) {
        User loggedUser = (User) request.getSession().getAttribute("loggedUser");

        serviceMediator.registerUser(loggedUser, new SimpleChatUserImpl(response, loggedUser));
    }


    private void sendMessage(HttpServletResponse httpServletResponse) {
        PrintWriter writer = null;
        try {
            writer = httpServletResponse.getWriter();
        } catch (IOException e) {
            e.printStackTrace();
        }


        writer.write("{\"msg\" : \"from-server\"}");
//        User loggedUser = (User) request.getSession().getAttribute("loggedUser");
//
//        serviceMediator.addChatMessage(loggedUser, "test message here");
    }

}
