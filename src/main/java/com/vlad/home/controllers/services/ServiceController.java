package com.vlad.home.controllers.services;

import com.vlad.home.model.User;
import com.vlad.home.services.UserService;
import com.vlad.home.support.patterns.chainservice.chainofresponsibility.EmailChain;
import com.vlad.home.support.patterns.chainservice.chainofresponsibility.SmsChain;
import com.vlad.home.support.exception.ShedulerExistException;
import com.vlad.home.support.factory.ServiceFactory;
import com.vlad.home.support.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Vlad Z
 */
@Controller
@PreAuthorize("isAuthenticated()")
public class ServiceController {

    @Autowired
    private ServiceFactory serviceFactory;

    @Autowired
    private JobRunner jobRunner;

    @Autowired
    private UserService userService;

    @Autowired
    @Qualifier("emailService")
    private Service emailService;

    @Autowired
    @Qualifier("smsService")
    private Service smsService;

    @RequestMapping("/services")
    public ModelAndView render(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView("services/choseservicepage");
        return modelAndView;
    }

    @RequestMapping("/processervice")
    public ModelAndView doService(HttpServletRequest request, HttpServletResponse response) {
        String serviceName = request.getParameter("selected-service");

        Service service = serviceFactory.getServiceByName(serviceName);
        org.springframework.security.core.userdetails.User principal = (org.springframework.security.core.userdetails.User)
                SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userService.getUserByEmail(principal.getUsername());
        try {
            service.send(user);
        } catch (Exception e) {
            e.printStackTrace();
        }


        ModelAndView modelAndView = new ModelAndView("services/choseservicepage");
        return modelAndView;
    }

    @RequestMapping("/runservicewithscheduler")
    public void runScheduler(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Service emailService = serviceFactory.getServiceByName("email");


        boolean success = true;
        String err = "";
        try {
            jobRunner.runScheduler(emailService);
        } catch (ShedulerExistException e) {
            success = false;
            err = e.getMessage();
        }

        response.setContentType("text/html");
        PrintWriter writer = null;

        writer = response.getWriter();

        StringBuilder sb = new StringBuilder();
        if (success) {
            sb.append("\"status\":\"runned\"")
                    .append(",")
                    .append("\"msg\":\"")
                    .append("Scheduler was run")
                    .append("\"");
        } else {
            sb.append("\"status\":\"error\"")
                    .append(",")
                    .append("\"msg\":\"")
                    .append(err)
                    .append("\"");

        }

        writer.write(sb.toString());
    }

    @RequestMapping("/runallservices")
    public void runAllServices() {
        EmailChain chain = new EmailChain(emailService);
        chain.setCandidate(new SmsChain(smsService));

        org.springframework.security.core.userdetails.User principal = (org.springframework.security.core.userdetails.User)
                SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        User user = userService.getUserByEmail(principal.getUsername());
        Long userId = user.getId();

        chain.startAction(userId);
    }

}
