package com.vlad.home.controllers.springform;

import com.vlad.home.controllers.BaseController;
import com.vlad.home.model.BankGovernment;
import com.vlad.home.model.CarProducer;
import com.vlad.home.model.GovernmentHistoryParameter;
import com.vlad.home.services.BankGovernmentService;
import com.vlad.home.services.CarGovernmentService;
import com.vlad.home.services.GovernmentHistoryService;
import com.vlad.home.support.generic.GovernmentCommonService;
import com.vlad.home.support.spring.form.GovernmentWrapper;
import com.vlad.home.support.spring.validation.GovernmentValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by vlad.
 */
@Controller
@PreAuthorize("isAuthenticated()")
@SessionAttributes({"parameter", "governmentWrapper"})
public class SpringConverterController extends BaseController {

    private final static Logger logger = Logger.getLogger(SpringConverterController.class);
    private final static String REDIRECT_TO_MAIN = "redirect:/springFormConvert";

    @Qualifier("carGovernmentServiceImpl")
    @Autowired
    private CarGovernmentService carService;
    @Qualifier("bankGovernmentServiceImpl")
    @Autowired
    private BankGovernmentService bankService;
    @Qualifier("governmentHistoryServiceImpl")
    @Autowired
    private GovernmentHistoryService historyService;
    @Qualifier("governmentCommonService")
    @Autowired
    private GovernmentCommonService commonService;

    @RequestMapping(value = "springFormConvert", method = RequestMethod.GET)
    public ModelAndView render(HttpServletRequest request, HttpServletResponse response) {

        ModelAndView modelAndView = new ModelAndView("government/view");


        //to show Government
        List<CarProducer> carProducers = carService.getAll();
        List<BankGovernment> bankGovernments = bankService.getAll();

        GovernmentHistoryParameter lastParameter = historyService.getLastParameter();
        modelAndView.addObject("parameter", lastParameter);
        GovernmentWrapper governmentWrapper = new GovernmentWrapper();
        modelAndView.addObject("governmentWrapper", governmentWrapper);

        governmentWrapper.setBankGovernments(bankGovernments);
        governmentWrapper.setCarProducers(carProducers);
        //to show Government


        return modelAndView;
    }

    @RequestMapping(value = "/show-government")
    public String showGovernment(HttpServletRequest request, HttpServletResponse response) {

        String type = request.getParameter("type");
        GovernmentHistoryParameter parameter = new GovernmentHistoryParameter();
        parameter.setSelected(new Date());
        parameter.setSelectedTypeHistory(type);

        historyService.persist(parameter);

        return REDIRECT_TO_MAIN;
    }

    @RequestMapping(value = "/addGovernment", method = RequestMethod.POST)
    public String addNewGovernment(HttpServletRequest request, HttpServletResponse response,
                                   @ModelAttribute("governmentWrapper") @Validated GovernmentWrapper wrapper, BindingResult validationResult) {

        if (validationResult.hasErrors()) {
            request.setAttribute("validationState", false);
            return "testingspringconvertion/view";
        }
        request.setAttribute("validationState", true);


        String type = wrapper.getType();
        String action = wrapper.getAction();

        if (type.equals("CARS")) {
            CarProducer carProducer = wrapper.getCarProducer();
            if (action.equals("Add")) {
                carService.persist(carProducer);
                logger.info(action + " > " + "new Car was added");
            } else {
                if (action.equals("Edit")) {
                    carService.update(carProducer);
                    logger.info(action + " > " + "Car was updated; car id=" + carProducer.getId());
                }
            }
        } else {
            if (type.equals("BANKS")){
                BankGovernment bankGovernment = wrapper.getBankGovernment();

                if (action.equals("Add")) {
                    bankService.persist(bankGovernment);
                    logger.info(action + " > " + "new Bank was added");
                } else {
                    if (action.equals("Edit")) {
                        bankService.update(bankGovernment);
                        logger.info(action + " > " + "Bank was updated; car id=" + bankGovernment.getId());
                    }
                }
            }
        }

        return REDIRECT_TO_MAIN;
    }

    @InitBinder("governmentWrapper")
    public void governmentBinder(WebDataBinder dataBinder) {
        dataBinder.setValidator(new GovernmentValidator());

        DateFormat dateFormat = new SimpleDateFormat("dd-M-yyyy");
        dataBinder.registerCustomEditor(Date.class,
                "bankGovernment.dateOfBase",
                new CustomDateEditor(dateFormat, false));
    }

    /*@InitBinder("studentWrapper")
    public void initBinderStudent(WebDataBinder webDataBinder) {

        webDataBinder.registerCustomEditor(Set.class, "student.learningSubjects", new CustomCollectionEditor(LinkedHashSet.class) {
            @Override
            public void setAsText(String text) throws IllegalArgumentException {
                HashSet<String> set = new HashSet<>();

                String regex = ";";
                String[] split = text.split(regex);
                for (int i = 0; i < split.length; i++) {
                    set.add(split[i]);
                }
                setValue(set);
            }

            @Override
            public String getAsText() {
                String res = "";
                Set<String> set = (Set) getValue();
                for (String item : set) {
                    res = res.concat(item);
                }
                return res;
            }
        });

        webDataBinder.registerCustomEditor(List.class, "student.friends", new CustomCollectionEditor(ArrayList.class) {
            @Override
            public void setAsText(String text) throws IllegalArgumentException {
                List<String> names = new ArrayList<>();
                String regex = ";";
                String[] split = text.split(regex);
                for (int i = 0; i < split.length; i++) {
                    names.add(split[i]);
                }

                setValue(names);
            }

            @Override
            public String getAsText() {
                String res = "";
                List<String> list = (List) getValue();
                for (String item : list) {
                    res = res.concat(item);
                }
                return res;
            }
        });

        webDataBinder.registerCustomEditor(Hostel.class, "student.hostel", new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) throws IllegalArgumentException {
                if (text.isEmpty()) {
                    return;
                }
                Long id = Long.valueOf(text);
                List<Hostel> availableHostels = new StudentSource().getAvailableHostels();
                for (Hostel availableHostel : availableHostels) {
                    if (availableHostel.getId().equals(id)) {
                        setValue(availableHostel);
                        break;
                    }
                }
            }
        });

        webDataBinder.registerCustomEditor(Boolean.class, "student.activeCheckBox", new PropertyEditorSupport() {

            @Override
            public void setAsText(String text) throws IllegalArgumentException {
                Boolean converted = Boolean.valueOf(text);
                setValue(converted);
            }
        });
    }

    private StudentWrapper addStubData(StudentWrapper wrapper) {
        Student student = new Student();
        wrapper.setStudent(student);

        student.setName("Pasha");
        student.setAge(22);
        List<String> friends = new ArrayList<>();
        friends.add("Dima");
        friends.add("Arkadii");
        friends.add("Grigorii");
        student.setFriends(friends);
        Set<String> subjects = new HashSet<>();
        subjects.add("Mathematics");
        subjects.add("Geomitry");
        subjects.add("Filosof");
        student.setLearningSubjects(subjects);
        student.setSalary(200.0);

        student.setWeekCash("100$");
        final Map<String, Boolean> selectedCourses = new HashMap<>();
        selectedCourses.put("Mathematics", Boolean.TRUE);
        selectedCourses.put("Geography", Boolean.TRUE);
        student.setSelectedCourses(selectedCourses);

        student.setHostel(wrapper.getStudentSource().getAvailableHostels().get(1));
        student.setActiveCheckBox(Boolean.TRUE);
        return wrapper;
    }*/
}
