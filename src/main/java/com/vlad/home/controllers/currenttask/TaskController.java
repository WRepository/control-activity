package com.vlad.home.controllers.currenttask;

import com.vlad.home.controllers.BaseController;
import com.vlad.home.model.CurrentTask;
import com.vlad.home.services.CurrentTaskService;
import com.vlad.home.services.impl.CurrentTaskServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.*;

/**
 * Created by Vlad.
 *
 */
@Controller
@PreAuthorize("isAuthenticated()")
public class TaskController extends BaseController{

    private final String UPDATE_PAGE = "redirect:/renderTask";

    @Autowired
    private CurrentTaskService taskService;


    @RequestMapping(value = "/renderTask")
    public ModelAndView render(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView("currenttask/view");

        List<CurrentTask> tasks = taskService.getAll();
        List<CurrentTask> mainTasks = taskService.getTasksByStatus(tasks, CurrentTaskServiceImpl.TaskState.NEW, CurrentTaskServiceImpl.TaskState.MEDIUM);
        modelAndView.addObject("tasks", mainTasks);
        
        List<CurrentTask> completedRejectedTasks = taskService.getTasksByStatus(tasks, CurrentTaskServiceImpl.TaskState.COMPLETED, CurrentTaskServiceImpl.TaskState.REJECTED);
        Collections.sort(completedRejectedTasks, new TasksComparator());
        modelAndView.addObject("completedRejectedTasks", completedRejectedTasks);

        return modelAndView;
    }

    @RequestMapping(value = "/addNewTask")
    public String addTask(HttpServletRequest request) throws UnsupportedEncodingException {
        request.setCharacterEncoding("utf-8");
        String newTask = request.getParameter("addedTextBody").trim();

        if (!newTask.isEmpty())
        taskService.addNewTask(newTask);

        return UPDATE_PAGE;
    }

    @RequestMapping(value = "/madeTask")
    public String taskWasMade(HttpServletRequest request) {

        String taskIdStr = request.getParameter("taskId");
        Long taskId = Long.valueOf(taskIdStr);

        taskService.taskWasMade(taskId);

        return UPDATE_PAGE;
    }

    @RequestMapping(value = "/markAsComplete")
    public String taskWasComplete(HttpServletRequest request) {

        String taskIdStr = request.getParameter("taskId");
        Long taskId = Long.valueOf(taskIdStr);

        taskService.markAsComplete(taskId);

        return UPDATE_PAGE;
    }

    @RequestMapping(path = "/rejectTask", method = RequestMethod.POST)
    public String rejectTask(HttpServletRequest request) {

        String taskIdStr = request.getParameter("taskId");
        String description = request.getParameter("descriptionRejected");
        Long taskId = Long.valueOf(taskIdStr);

        CurrentTask rejectedTask = taskService.getEntityById(taskId);
        if (rejectedTask.getState().equals(CurrentTaskServiceImpl.TaskState.NEW)) {
            rejectedTask.setState(CurrentTaskServiceImpl.TaskState.REJECTED);
            rejectedTask.setFinishDate(new Date());
            rejectedTask.setRejectedDescription(description);

            taskService.update(rejectedTask);
        }

        return UPDATE_PAGE;
    }



    private class TasksComparator implements Comparator<CurrentTask> {

        @Override
        public int compare(CurrentTask o1, CurrentTask o2) {
            return o2.getFinishDate().compareTo(o1.getFinishDate());
        }

    }

}
