package com.vlad.home.dao;

import com.vlad.home.model.ApplicationImage;

import java.util.List;

/**
 * Created by Vlad.
 *
 */
public interface ApplicationImageDAO extends BaseDAO<ApplicationImage, Long>{

    void deleteJDBCPage();

    List<ApplicationImage> getImagesByCategory(String category);

}
