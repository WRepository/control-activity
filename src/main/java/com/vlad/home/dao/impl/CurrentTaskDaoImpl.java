package com.vlad.home.dao.impl;

import com.vlad.home.dao.CurrentTaskDao;
import com.vlad.home.model.CurrentTask;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Vlad.
 *
 */
@Repository
public class CurrentTaskDaoImpl extends BaseDAOImpl<CurrentTask, Long> implements CurrentTaskDao {

    public CurrentTaskDaoImpl() {
        super(CurrentTask.class);
    }

    @Override
    public List<CurrentTask> getAll() {
        return currentSession().createQuery("from CurrentTask").list();
    }
}
