package com.vlad.home.dao.impl;

import com.vlad.home.dao.GovernmentHistoryDao;
import com.vlad.home.model.GovernmentHistoryParameter;
import com.vlad.home.services.GovernmentHistoryService;
import com.vlad.home.services.impl.BaseServiceImpl;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Vlad.
 *
 */
@Service
@Transactional
public class GovernmentHistoryServiceImpl extends BaseServiceImpl<GovernmentHistoryParameter, Long> implements GovernmentHistoryService {

    private static final Logger LOGGER = Logger.getLogger(GovernmentHistoryServiceImpl.class);

    @Qualifier("governmentHistoryDaoImpl")
    @Autowired
    private GovernmentHistoryDao historyDao;


    public GovernmentHistoryServiceImpl() {
        super(GovernmentHistoryParameter.class);
    }

    @Override
    public GovernmentHistoryParameter getLastParameter() {
        LOGGER.info("Trying get recent GovernmentHistoryParameter.");
        return historyDao.getLastParameter();
    }

}
