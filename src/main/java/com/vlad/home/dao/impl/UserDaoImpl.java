package com.vlad.home.dao.impl;

import com.vlad.home.dao.UserDao;
import com.vlad.home.model.User;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserDaoImpl extends BaseDAOImpl<User, Long> implements UserDao {

    public UserDaoImpl() {
        super(User.class);
    }

    @Override
    public List<User> getAllUsers() {
            List<User> list =(List<User>) currentSession().createQuery("from User ").list();
        return list;
    }

    @Override
    public User getUserByName(String name) {
        return null;
    }

    @Override
    public User getUserByEmail(String email) {
        String hql = "FROM User u WHERE u.emailAdress = :emailAdress";
        Query query = currentSession().createQuery(hql);
        query.setParameter("emailAdress", email);
        User user = (User) query.uniqueResult();
        return user;
    }
}
