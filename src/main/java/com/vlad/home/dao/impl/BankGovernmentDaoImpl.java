package com.vlad.home.dao.impl;

import com.vlad.home.dao.BankGovernmentDao;
import com.vlad.home.model.BankGovernment;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Vlad.
 *
 */
@Repository
public class BankGovernmentDaoImpl extends BaseDAOImpl<BankGovernment, Long> implements BankGovernmentDao {

    public BankGovernmentDaoImpl() {
        super(BankGovernment.class);
    }

    @Override
    public List<BankGovernment> getAll() {
        return currentSession().createQuery("FROM BankGovernment").list();
    }
}
