package com.vlad.home.dao.impl;

import com.vlad.home.dao.BaseDAO;
import com.vlad.home.model.BaseModel;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Vlad.
 */
@Repository("BaseDaoImpl")
@Transactional
public class BaseDAOImpl<T extends BaseModel, PK extends Serializable> extends HibernateDaoSupport implements BaseDAO<T, PK> {

    private Class<T> persistenceClass;

    public BaseDAOImpl(Class<T> persistenceClass) {
        this.persistenceClass = persistenceClass;
    }

    public BaseDAOImpl() {}

    @Autowired
    public void init(SessionFactory sessionFactory) {
        setSessionFactory(sessionFactory);
    }

    @Override
    public void save(T entity) {
        currentSession().save(entity);
    }

    @Override
    public void persist(T entity) {
        currentSession().persist(entity);
    }

    @Override
    public void saveOrUpdate(T entity) {
        currentSession().saveOrUpdate(entity);
    }

    @Override
    public T merge(T entity) {
        return (T) currentSession().merge(entity);
    }

    @Override
    public T getEntityById(PK id) {
        return currentSession().get(persistenceClass, id);
    }

    @Override
    public T getEntityById(PK id, Class<T> persistence) {
        return currentSession().get(persistence, id);
    }

    @Override
    public List<T> getAll(Class<T> persistenceClass) {
        return currentSession().createCriteria(persistenceClass).list();
    }

    @Override
    public List<T> getAll() {
        return  currentSession().createCriteria(persistenceClass).list();
    }

    @Override
    public void update(T entity) {
        currentSession().update(entity);
    }

    @Override
    public void delete(T entity) {
        currentSession().delete(entity);
    }

    @Override
    public void test(T entity) {

    }
}
