package com.vlad.home.dao.impl;

import com.vlad.home.dao.CarGovernmentDao;
import com.vlad.home.model.CarProducer;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Vlad.
 *
 */
@Repository
public class CarGovernmentDaoImpl extends BaseDAOImpl<CarProducer, Long> implements CarGovernmentDao {

    public CarGovernmentDaoImpl() {
        super(CarProducer.class);
    }

    @Override
    public List<CarProducer> getAll() {
        return currentSession().createQuery("FROM CarProducer").list();
    }
}
