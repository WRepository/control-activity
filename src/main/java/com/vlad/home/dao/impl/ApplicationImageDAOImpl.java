package com.vlad.home.dao.impl;

import com.vlad.home.dao.ApplicationImageDAO;
import com.vlad.home.model.ApplicationImage;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Vlad.
 *
 */
@Repository
public class ApplicationImageDAOImpl extends BaseDAOImpl<ApplicationImage, Long> implements ApplicationImageDAO {

    public ApplicationImageDAOImpl() {
        super(ApplicationImage.class);
    }

    @Override
    public void deleteJDBCPage() {
        String deleteQuery = "DELETE FROM app_images WHERE page_view='OR-page'";

        currentSession().createSQLQuery(deleteQuery).executeUpdate();
    }

    @Override
    public List<ApplicationImage> getImagesByCategory(String cat) {
        Query query = currentSession().createQuery("FROM ApplicationImage ai WHERE ai.category=:cat");
        query.setParameter("cat", cat);

        return query.list();
    }
}
