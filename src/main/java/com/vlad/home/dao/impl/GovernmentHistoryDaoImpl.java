package com.vlad.home.dao.impl;

import com.vlad.home.dao.GovernmentHistoryDao;
import com.vlad.home.model.GovernmentHistoryParameter;
import org.springframework.stereotype.Repository;

/**
 * Created by Vlad.
 *
 */
@Repository
public class GovernmentHistoryDaoImpl extends BaseDAOImpl<GovernmentHistoryParameter, Long> implements GovernmentHistoryDao {

    public GovernmentHistoryDaoImpl() {
        super(GovernmentHistoryParameter.class);
    }

    @Override
    public GovernmentHistoryParameter getLastParameter() {
        String query = "FROM GovernmentHistoryParameter par " +
                "WHERE par.selected = " +
                "(SELECT MAX(par.selected) FROM GovernmentHistoryParameter par)";
        GovernmentHistoryParameter result = (GovernmentHistoryParameter) currentSession().createQuery(query).uniqueResult();
        if (result == null) result = new GovernmentHistoryParameter();
        return result;
    }

}
