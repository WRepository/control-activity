package com.vlad.home.dao;

import com.vlad.home.model.GovernmentHistoryParameter;

/**
 * Created by Vlad.
 *
 */
public interface GovernmentHistoryDao {

    GovernmentHistoryParameter getLastParameter();
}
