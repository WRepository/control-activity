package com.vlad.home.dao;

import com.vlad.home.model.CurrentTask;

import java.util.List;

/**
 * Created by Vlad.
 *
 */
public interface CurrentTaskDao extends BaseDAO<CurrentTask, Long>{

    List<CurrentTask> getAll();
}
