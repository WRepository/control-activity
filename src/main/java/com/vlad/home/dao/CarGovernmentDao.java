package com.vlad.home.dao;

import com.vlad.home.model.CarProducer;

import java.util.List;

/**
 * Created by Vlad.
 *
 */
public interface CarGovernmentDao {
    List<CarProducer> getAll();
}
