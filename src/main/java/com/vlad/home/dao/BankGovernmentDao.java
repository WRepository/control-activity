package com.vlad.home.dao;

import com.vlad.home.model.BankGovernment;

import java.util.List;

/**
 * Created by Vlad.
 *
 */
public interface BankGovernmentDao {
    List<BankGovernment> getAll();
}
