package com.vlad.home.dao;

import com.vlad.home.model.BaseModel;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Vlad.
 *
 */
public interface BaseDAO<T extends BaseModel, PK extends Serializable> {
    void save(T entity);

    void saveOrUpdate(T entity);

    T getEntityById(PK id);

    T getEntityById(PK id, Class<T> persistence);

    void update(T entity);

    void delete(T entity);

    void persist(T entity);

    T merge(T entity);

    List<T> getAll();

    List<T> getAll(Class<T> persistenceClass);

    void test(T entity);
}
