package com.vlad.home.dao;

import com.vlad.home.model.User;

import java.util.List;

/**
 * Created by vlad.
 */
public interface UserDao extends BaseDAO<User, Long>{

    List<User> getAllUsers();

    User getUserByName(String name);

    User getUserByEmail(String email);


}
