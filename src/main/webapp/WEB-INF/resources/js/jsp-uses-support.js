/**
 * Created by vlad.
 */

function showHideElementBlock(elementId) {
    var divId = '#'.concat(elementId);

    var propertyCSSValue = $(divId).css('display');
    if (propertyCSSValue !='none') {
        $(divId).css({'display':'none'})
    } else {
        $(divId).css({'display':'block'})
    }
}

function showElement(elementId) {
    var elementIdFull = '#'.concat(elementId);
    var propertyCSSValue = $(elementIdFull).css('display');
    if (propertyCSSValue == 'none') {
        $(elementIdFull).css({'display': 'block'})
    }
}

function hideElement(element) {
    var propertyCSSValue = $(element).css('display');
    if (propertyCSSValue == 'none') {
        $(element).css({'display': 'block'})
    }
}

function hideAllExcept(pattern, elExcept) {
    $(pattern).each(function (index, value) {
        $(value).css({'display': 'none'})
    });

    hideElement(elExcept);
}

function showAll(pattern) {
    $(pattern).each(function (index, value) {
        $(value).css({'display': 'block'})
    });
}

function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}

