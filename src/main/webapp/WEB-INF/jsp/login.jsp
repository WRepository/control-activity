<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/lib_holder_page.jsp"/>

<!DOCTYPE html>
<html lang="ru,en">
<head>
    <title>Авторизация</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="main">
            <div class="login-or">
                <hr class="hr-or">
            </div>
            <form id="login" role="form" method="POST" action="<c:url value="/check_user"/>">
                <div class="form-group">
                    <label for="username">Email</label>
                    <input type="email" autofocus class="form-control" name="username" id="username" tabindex="1">
                </div>
                <div class="form-group">
                    <label for="pass">Пароль</label>
                    <input type="password" class="form-control" name="password" id="pass" tabindex="2">
                </div>
                <button type="submit" class="btn btn btn-primary" tabindex="3">Войти</button>

                <button type="button" class="btn btn-primary pull-right" onclick="processRegister()" tabindex="4">Зарегистрироваться</button>
            </form>

            <c:if test="${failed}">
                <div style="margin-top: 20px" class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <strong>Невозможно авторезироваться</strong> Неправильный Логин или Пароль.
                </div>
            </c:if>
        </div>
    </div>
</div>

</body>
</html>
<script type="text/javascript">
    $('#login').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            username: {
                validators: {
                    notEmpty: {
                        message: 'Введите e-mail'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'Введите пароль'
                    }
                }
            }
        }
    });

    function processRegister() {
        window.location='${pageContext.request.contextPath}/register';
    }
</script>
<style>
    body {
        padding-top: 20px;
        font-size: 12px
    }

    .main {
        max-width: 320px;
        margin: 6% auto 0;
    }

    .login-or {
        position: relative;
        font-size: 18px;
        color: #aaa;
        margin-top: 10px;
        margin-bottom: 10px;
        padding-top: 10px;
        padding-bottom: 10px;
    }

    .hr-or {
        background-color: #cdcdcd;
        height: 1px;
        margin-top: 0 !important;
        margin-bottom: 0 !important;
    }

    h3 {
        text-align: center;
        line-height: 300%;
    }
</style>
