<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../lib_holder_page.jsp"/>


<div class="mainName">
    <h2>Форма Регистрации</h2>
</div>

<spring:form id="registerUser" class="form-horizontal" action="/registerUser" modelAttribute="newUser" method="post">
    <div class="container">
        <div class="content">
            <div class="form-group required">
                <label class="col-sm-3 control-label" for="name">Имя</label>
                <div class="col-sm-8">
                    <spring:input id="name" path="userName" type="text" class="form-control" placeholder="Name"/>
                </div>
            </div>
            <div class="form-group required">
                <label class="col-sm-3 control-label" for="surName">Фамилия</label>
                <div class="col-sm-8">
                    <spring:input id="surName" path="surName" type="text" class="form-control" placeholder="Surname"/>
                </div>
            </div>
            <div class="form-group required">
                <label class="col-sm-3 control-label" for="mail">E-mail</label>
                <div class="col-sm-8">
                    <spring:input id="mail" path="emailAdress" type="email" class="form-control" placeholder="E-mail"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="birthday-date">Дата Рождения</label>
                <div class="col-sm-8">
                    <spring:input id="birthday-date" path="dateBirthday" type="text" class="form-control" placeholder="Birthday Date"/>
                </div>
            </div>
            <div class="form-group required">
                <label class="col-sm-3 control-label" for="password">Пароль</label>
                <div class="col-sm-5">
                    <spring:input id="password" path="password" type="password" class="form-control" placeholder="Password"/>
                </div>
            </div>
            <div class="form-group required">
                <label class="col-sm-3 control-label" for="password">Подтверждение</label>
                <div class="col-sm-5">
                    <spring:input name="confirmPassword" path="" type="password" class="form-control" placeholder="Password confirmation"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="descr">Дополнительно</label>
                <div class="col-sm-8">
                    <spring:textarea id="descr" path="description" rows="4" class="form-control" placeholder="Description"/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3"></div>
                <div class="col-sm-8">
                     <spring:button type="submit" class="btn btn-primary form-control pull-right">Зарегистрироваться</spring:button>
                </div>
            </div>
        </div>
    </div>
</spring:form>

<script type="text/javascript">
    $(document).ready(function () {
        $('#birthday-date').datepicker({
            format: 'dd-mm-yyyy',
            setDate: new Date(),
            autoclose: true
        });
    });
    /*form validation*/
    $(document).ready(function() {
        $('#registerUser').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                userName:{
                    validators: {
                        notEmpty: {
                            message: 'Имя обезательно для ввода'
                        }
                    }
                },
                surName:{
                    validators: {
                        notEmpty: {
                            message: 'Фамилия обезательна для ввода'
                        }
                    }
                },
                emailAdress:{
                    validators: {
                        notEmpty: {
                            message: 'E-mail обезателен для ввода'
                        },
                        remote: {
                            message: 'Введенный e-mail уже существует в БД',
                            url: '/checkExistEmail?${_csrf.parameterName}=${_csrf.token}',
                            type: 'POST',
                            delay: 1500
                        }
                    }
                },
                password:{
                    validators: {
                        notEmpty: {
                            message: 'Пароль обезателен для ввода'
                        }
                    }
                },
                confirmPassword: {
                    validators: {
                        notEmpty: {
                            message: 'Подтверждение пароля обезательно для ввода'
                        },
                        identical: {
                            field: 'password',
                            message: 'Пароли не совпадают'
                        }
                    }
                }
            }
        });
    });
</script>


<style type="text/css">
    .mainName{
        text-align: center;
        margin: 30px auto 50px auto;
    }
    .content {
        width: 600px;
        height: 300px;
        margin: auto;
    }
    .form-group.required .control-label:after {
        content:"*";
        color:red;
    }
</style>