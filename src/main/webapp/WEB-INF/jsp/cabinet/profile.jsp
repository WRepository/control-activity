<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring-tags" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spring-form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<script type="text/javascript" src="../../resources/js/jsp-uses-support.js"></script>
<link href="<c:url value="${pageContext.request.contextPath}resources/static/jssor/arrows.css"/>" type="text/css" rel="stylesheet"/>
<link href="<c:url value="${pageContext.request.contextPath}resources/static/jssor/thumbnail.css"/>" type="text/css" rel="stylesheet"/>


<jsp:include page="../navbar.jsp"/>

<div id="main" name="main" class="common-container">

    <div class="row">
        <div class="col-sm-8  custom-row">
            <h4>Выберите фото для загрузки</h4>
            <form id="fileForm" class="form-horizontal" method="post" action="/saveAllImages" enctype="multipart/form-data">
                <div class="form-group col-xs-8">

                        <input type="file" class="form-control" name="photos" multiple/>

                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary pull-right" value="Сохранить">
                </div>
            </form>
        </div>
    </div>

    <c:if test="${not empty privateData.hImages}">
        <div id="slider_container" class="slider-container">
            <!-- Slides Container -->
            <div u="slides" class="slides">
                <c:forEach items="${privateData.hImages}" var="hImage">
                    <!-- Loading Screen -->
                    <div data-u="loading" style="position: absolute; top: 0px; left: 0px; width: 800px; height: 356px; display: none;">
                        <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                        <div style="position:absolute;display:block;background:url(/resources/static/jssor/img/loading.gif) no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
                    </div>

                    <div>
                        <img u="image" src="/showLawImage?imageId=${hImage.id}"/>
                        <img u="thumb" src="/showLawImage?imageId=${hImage.id}"/>
                    </div>

                </c:forEach>
            </div>

            <!--#region Arrow Navigator Skin Begin -->
            <!-- Arrow Left -->
                       <span u="arrowleft" class="jssora09l" style="top: 125px; left: 8px;" autocenter="2">
                       </span>
            <!-- Arrow Right -->
            <span u="arrowright" class="jssora09r" style="top: 125px; right: 8px;" autocenter="2">
            </span>
            <!--#endregion Arrow Navigator Skin End -->

            <!-- thumbnail navigator container -->
            <div u="thumbnavigator" class="jssort01"
                 style="width: 800px; height: 100px; left:0px; bottom: 0px;">
                <!-- Thumbnail Item Skin Begin -->
                <div u="slides" style="cursor: move;">
                    <div u="prototype" class="p" style="position: absolute; width: 72px; height: 72px; top: 0; left: 0;">
                        <div class=w><div u="thumbnailtemplate" style="width: 100%; height: 100%; border: none;position:absolute; top: 0; left: 0;"></div></div>
                        <div class=c>
                        </div>
                    </div>
                </div>
                <!-- Thumbnail Item Skin End -->
            </div>
            <!-- Jssor Slider End -->
        </div>
    </c:if>


</div>

<script type="text/javascript">
    jQuery(document).ready(function ($) {

        var showSlider = '${not empty privateData.hImages}';

        if(showSlider == 'true') {

            var options = {
                $AutoPlay: false,
                $FillMode: 5,
                $ArrowNavigatorOptions: {
                    $Class: $JssorArrowNavigator$,
                    $ChanceToShow: 2
                },
                $ThumbnailNavigatorOptions: {
                    $Class: $JssorThumbnailNavigator$,              //[Required] Class to create thumbnail navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $Cols: ${privateData.hImages.size()},           //count of items
                    $Loop: 1,                                       //[Optional] Enable loop(circular) of carousel or not, 0: stop, 1: loop, 2 rewind, default value is 1
                    $Rows: 1,                                       //count of rows
                    $ActionMode: 1,                                 //better then others
                    $SpacingX: 6,                                   //[Optional] Horizontal space between each thumbnail in pixel, default value is 0
                    $SpacingY: 6,                                   //[Optional] Vertical space between each thumbnail in pixel, default value is 0
                    $ParkingPosition: 0,                            //-
                    $Orientation: 1,                                //0-vertical; 1-horizontal
                    $AutoCenter: 1                                  //-
                }
            };

            var jssor_slider1 = new $JssorSlider$('slider_container', options);
        }

    });
    /*File validation*/
    $(document).ready(function() {
        $('#fileForm').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                photos: {
                    validators: {
                        notEmpty: {
                            message: 'Выберете одно или несколько фото'
                        },
                        file: {
                            extension: 'jpeg,jpg,png',
                            type: 'image/jpeg,image/png',
                            message: 'Допустимые форматы jpg, jpeg, png. </br>Ограничения по размеру отсутствуют.'
                        }
                    }
                }
            }
        });
    });
</script>

<style type="text/css">
    .slider-container{
        position: relative;
        top: 150px;
        width: 800px;
        height: 456px;
        margin-left: auto;
        margin-right: auto;
    }
    .slides{
        cursor: move;
        position: absolute;
        overflow: hidden;
        left: 0px;
        top: 0px;
        width: 800px;
        height: 456px;
    }
    .common-container{
        position: relative;
        width: 800px;
        height: 600px;
        margin-left: auto;
        margin-right: auto;
    }
    .custom-row{
        margin: 0 auto;
        float: none !important;
    }
</style>