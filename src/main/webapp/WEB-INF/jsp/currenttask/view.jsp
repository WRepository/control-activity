<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="/WEB-INF/jsp/headers.jsp"/>


<div id="task-container-wrapper" class="container-task-wrapper">
        <c:if test="${tasks.size() ge 1}">
            <div id="task-container" class="container-task">
            <c:forEach items="${tasks}" var="currentTask" varStatus="status">
                <c:choose>
                    <c:when test="${currentTask.state.state eq 'NEW'}">
                        <form id="task-form" name="tasks" action="/madeTask" method="post">
                            <div class="task-row">
                                <div class="state-container-new">${currentTask.state.state}</div>
                                <div class="checkbox-container"><input type="checkbox" name="taskId" value="${currentTask.id}" required/></div>
                                <div class="text-container">${currentTask.text}</div>
                                <div class="tasks-button-container" ><button id="submit-button" type="submit">Task was made</button></div>
                                </br>
                            </div>
                        </form>
                    </c:when>
                    <c:when test="${currentTask.state.state eq 'MEDIUM'}">
                        <div class="task-row">
                            <div class="state-container-middle">${currentTask.state.state}</div>
                            <div class="text-container">${currentTask.text}</div>
                            <div><a class="href-container" href="/markAsComplete?taskId=${currentTask.id}">mark as completed</a></div>
                        </div>
                    </c:when>
                </c:choose>
            </c:forEach>
            </div>
        </c:if>

    <div id="add-new" class="container-add-task">
        <form class="form-horizontal" action="/addNewTask" method="post" accept-charset="UTF-8">
            <div class="form-group">
                <div class="col-xs-8">
                    <label for="text-name-body">Enter New Task:</label>
                </div>
                <div class="col-xs-6">
                    <textarea class="form-control" rows="2" id="text-name-body" name="addedTextBody" required></textarea>
                </div>
                <div class="col-xs-2">
                    <button class="btn btn-primary" type="submit">Add New Task</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div id="showCompletedTask" class="button-container">
    <button type="button" id="show-button" onclick="showCompletedTasks()">Show Completed Tasks</button>
</div>
<c:if test="${completedRejectedTasks.size() > 0}">
    <div id="task-container-completed" class="container-task-completed">
        <c:forEach items="${completedRejectedTasks}" var="currentTask" varStatus="status">
            <fmt:formatDate value="${currentTask.finishDate}" var="formatDate" pattern="d.MM"/>
            <fmt:formatDate value="${completedRejectedTasks[status.index +1].finishDate}" var="formatDateNextTask" pattern="d.MM"/>

            <div class="task-row">
                <c:choose>
                    <c:when test="${currentTask.state.state eq 'COMPLETED'}">
                        <div class="state-container-completed">${currentTask.state.state}</div>
                    </c:when>
                    <c:otherwise>
                        <div class="state-container-rejected">${currentTask.state.state}</div>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${status.last ne true}">
                        <c:if test="${formatDate == formatDateNextTask}">
                            <c:if test="${currentTask.state.state != completedRejectedTasks[status.index + 1].state.state}">
                                <div id="dateArea" class="dateArea">${formatDate}</div>
                            </c:if>
                            <c:if test="${currentTask.state.state eq completedRejectedTasks[status.index + 1].state.state}">
                                <div id="dateArea" class="dateAreaBridge">${formatDate}</div>
                            </c:if>
                        </c:if>
                        <c:if test="${formatDate != formatDateNextTask}">
                            <div id="dateArea" class="dateArea">${formatDate}</div>
                        </c:if>
                    </c:when>
                    <c:otherwise>
                        <div id="dateArea" class="dateArea">${formatDate}</div>
                    </c:otherwise>
                </c:choose>
                <div class="text-container-completed">${currentTask.text}</div>
            </div>

            <%--Checking to show missed working day--%>
            <c:if test="${status.last ne true}">
                <c:if test="${formatDate != formatDateNextTask}">
                    <c:set var="checkedTimeMills" value="${completedRejectedTasks[status.index +1].finishDate.time + (60 * 60 * 24 * 1000)}"/>
                    <jsp:useBean id="checkedTime" class="java.util.Date"/>
                    <jsp:setProperty name="checkedTime" property="time" value="${checkedTimeMills}"/>

                    <fmt:formatDate value="${checkedTime}" pattern="d.MM" var="formatDateNext"/>

                    <%--day off checking--%><fmt:formatDate value="${currentTask.finishDate}" var="isDaysOff" pattern="E"/>
                    <c:choose>
                        <c:when test="${formatDateNext != formatDate}">
                            <c:if test="${isDaysOff != 'Mon'}">
                                <%--add row for each missed day--%>
                                <c:set var="stopLoop" value="false" scope="page"/>
                                <c:forEach begin="${status.index}" items="${completedRejectedTasks}" var="currentTask2">
                                    <c:if test="${!stopLoop}">
                                        <c:if test="${currentTask2.state.state eq 'COMPLETED'}">
                                            <c:set value="${currentTask2.finishDate.time - (60 * 60 * 24 * 1000)}" var="decreaseTime"/>
                                            <c:set var="stopLoop" value="true"/>
                                        </c:if>
                                    </c:if>
                                </c:forEach>
                                <c:forEach begin="0" end="10" step="1">
                                    <jsp:useBean id="prevDateBean" class="java.util.Date"/>
                                    <jsp:setProperty name="prevDateBean" property="time" value="${decreaseTime}"/>
                                    <fmt:formatDate value="${prevDateBean}" var="prevDate" pattern="d.MM"/>

                                    <c:if test="${formatDateNextTask != prevDate}">
                                        <div class="missed-working-day-row">
                                            <span>No One Task was Done</span>
                                            <span class="missed-working-day-date">Date: ${prevDate}</span>
                                        </div>
                                        <c:set var="decreaseTime" value="${decreaseTime - (60 * 60 * 24 * 1000)}"/>
                                        <jsp:setProperty name="prevDateBean" property="time" value="${decreaseTime}"/>
                                    </c:if>
                                </c:forEach>
                                <%--add row for each missed day--%>
                            </c:if>
                        </c:when>
                        <c:when test="${completedRejectedTasks[status.index +1].state.state eq 'REJECTED'}">
                            <c:if test="${isDaysOff != 'Mon'}">
                                <div class="missed-working-day-row">
                                    <span>No One Task was Done</span>
                                    <span class="missed-working-day-date">Date: ${formatDateNextTask}</span>
                                </div>
                            </c:if>
                        </c:when>
                    </c:choose>
                </c:if>
            </c:if>
            <%--Checking to show missed working day--%>

        </c:forEach>
    </div>
</c:if>


<script type="text/javascript">
    function showCompletedTasks() {
        $('#task-container-completed').css({'display': "block"});
        $('#showCompletedTask').css({'display': 'none'});
    }



</script>

<style type="text/css">
    .container-task-wrapper{
        float: left;
        width: 58%;
    }

    .container-task {
        margin: 10px;
        border: 1px solid;
        padding-left: 10px;
        padding-top: 10px;
    }

    .container-task-completed{
        margin: 10px;
        border: 1px solid;
        width: auto;
        padding-left: 10px;
        padding-top: 10px;
        float: left;
        display: none;
    }
    .container-add-task{
        margin: 10px;
    }
    .checkbox-container{
        width: 40px;
        float: left;
        margin-left: 10px;
    }

    .text-container{
        float: left;
        margin-left: 10px;
        max-width: 670px
    }
    .text-container-completed{
        float: left;
        margin-left: 10px;
        width: 450px;
    }
    .tasks-button-container{
        float: left;
    }

    .task-rejected-container{
        float: right;
        background-color: rgb(167, 73, 73);
        height: 18px;
        cursor: pointer;
        text-overflow: ellipsis;
        overflow: hidden;
    }
    .tasks-rejected-container-normal {
        width: 27px;
    }
    .tasks-rejected-container-hover {
        width: 101px;
    }

    .href-container{
        margin-left: 10px;
    }
    .state-container-new{
        background-color: silver;
        padding: 10px;
        border-radius: 12px;
        float: left;

    }
    .state-container-middle{
        background-color: yellow;
        padding: 10px;
        border-radius: 12px;
        float: left;
    }
    .state-container-completed{
        background-color: darkgreen;
        padding: 10px;
        border-radius: 12px;
        float: left;
    }
    .state-container-rejected{
        background-color: rgb(167, 73, 73);
        padding: 10px;
        border-radius: 12px;
        float: left;
    }
    .task-row{
        position: relative;
        height: 50px;
    }
    .task-row button {
        margin-left: 40px;
    }

    .dateArea{
        background-color: silver;
        float: left;
        margin-left: 10px;
        padding: 10px;
    }
    .dateAreaBridge{
        background-color: silver;
        float: left;
        margin-left: 10px;
        padding: 10px;
        height: 30px;
    }
    .missed-working-day-date{
        float: right;
    }
    .button-container {
        margin: 10px 0 0 500px;
        float: left;
    }
    .missed-working-day-row {
        background-color: rgb(167, 73, 73);
        padding: 10px;
        margin-bottom: 10px;
    }


</style>