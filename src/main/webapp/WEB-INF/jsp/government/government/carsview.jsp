<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="spring-form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<table id="carsTable" class="table table-bordered table-striped display" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>type</th>
        <th>Summary Employee count</th>
        <th>Filials</th>
        <th>Models</th>
        <th>Actions</th>

    </tr>
    </thead>
    <tbody>
    <c:forEach var="car" items="${governmentWrapper.carProducers}">
        <tr>
            <td id="silent-car-id">
                    ${car.id}
            </td>
            <td id="silent-car-name">
                    ${car.name}
            </td>
            <td id="silent-car-type">
                    ${car.type}
            </td>
            <td id="silent-car-countOfEmployees">
                    ${car.countOfEmployees}
            </td>
            <td id="silent-rawOpenedFilials">
                    ${car.rawOpenedFilials}
            </td>
            <td id="silent-rasProducedModels">
                    ${car.rawProducedModels}
            </td>
            <td>
                <button type="button" onclick="governmentEditingProcess('car', ${status.index})" class="btn btn-primary">Edit</button></td>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<script type="text/javascript">
    $(document).ready(function(){
        $('#carsTable').DataTable();
    })
</script>