<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring-form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<table id="bankTable" class="table table-bordered table-striped display" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>type</th>
        <th>Summary Employee count</th>
        <th>Created Date</th>
        <th>Bosses</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    <%--@elvariable id="bankGovernments" type="java.util.List<com.vlad.home.model.BankGovernment>"--%>
    <c:forEach var="bank" items="${governmentWrapper.bankGovernments}" varStatus="statusLoop">
            <tr>
                <td>
                    <span id="id">${bank.id}</span>
                </td>
                <td>
                   <span id="row-name-${statusLoop.index}">${bank.name}</span>
                </td>
                <td>
                    <span id="type">${bank.type}</span>
                </td>
                <td>
                    <span id="row-countOfEmployees-${statusLoop.index}">${bank.countOfEmployees}</span>
                </td>
                <fmt:formatDate value="${bank.dateOfBase}" pattern="dd-MM-yyyy" var="formatDate"/>
                <td>
                    <span id="row-formatDate-${statusLoop.index}">${formatDate}</span>
                </td>
                <td>
                   <span id="row-headMens-${statusLoop.index}">${bank.headMens}</span>
                </td>
                <td>
                    <button type="button" onclick="governmentEditingProcess(${bank.id}, ${statusLoop.index})" name="submit-butt" class="btn btn-primary">Edit</button>
                    <button type="button" onclick="" class="btn btn-danger">Remove</button>
                </td>
            </tr>
    </c:forEach>
    </tbody>
</table>

<script type="text/javascript">
    $(document).ready(function () {
        $('#bankTable').dataTable({
            bAutoWidth: false ,
            aoColumns : [
                { "sWidth": "5%"},
                { "sWidth": "25%"},
                { "sWidth": "15%"},
                { "sWidth": "15%"},
                { "sWidth": "15%"},
                { "sWidth": "15%"},
                { "sWidth": "10%"},
            ]
        });
    });
</script>