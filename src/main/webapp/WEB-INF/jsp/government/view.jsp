<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring-tags" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spring-form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<script type="text/javascript" src="resources/js/jsp-uses-support.js"></script>

<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/headers.jsp"/>

<c:set var="governmentType" value="${parameter.selectedTypeHistory eq 'BANKS' ? 'bankGovernment' : 'carProducer'}" scope="request"/>


<%--TO SHOW GOVERNMENT--%>
<h1>Government</h1>

<div id="header-container" class="header-container">
    <c:choose>
        <c:when test="${parameter.selectedTypeHistory eq 'CARS'}">
            <div class="gov-header selected">
                <a href="show-government?type=CARS">CARS</a></div>
            <div class="gov-header ">
                <a href="show-government?type=BANKS">BANKS</a></div>
        </c:when>
        <c:when test="${parameter.selectedTypeHistory eq 'BANKS'}">
            <div class="gov-header ">
                <a href="show-government?type=CARS">CARS</a></div>
            <div class="gov-header selected">
                <a href="show-government?type=BANKS">BANKS</a></div>
        </c:when>
        <c:otherwise>
            <div class="gov-header ">
                <a href="show-government?type=CARS">CARS</a></div>
            <div class="gov-header ">
                <a href="show-government?type=BANKS">BANKS</a></div>
        </c:otherwise>
    </c:choose>
</div>


<c:choose>
    <c:when test="${parameter.selectedTypeHistory eq 'CARS'}">
        <jsp:include page="government/carsview.jsp"/>
    </c:when>
    <c:when test="${parameter.selectedTypeHistory eq 'BANKS'}">
        <jsp:include page="government/banks.jsp"/>
    </c:when>
    <c:otherwise>
        <h2>Not category to show</h2>
    </c:otherwise>
</c:choose>


<button id="add-form" type="button" onclick="showHideElementBlock('add-form-${parameter.selectedTypeHistory}')" class="btn btn-danger">Add ${parameter.selectedTypeHistory} Record</button>

<div id="form-wrapper" class="form-wrapper">
    <div id="add-form-${parameter.selectedTypeHistory}" class="add-form">
        <spring-form:form id="add-government-form" modelAttribute="governmentWrapper" action="/addGovernment" method="post" class="form-horizontal">
            <spring-form:hidden path="type" value="${parameter.selectedTypeHistory}"/>

            <spring-form:hidden path="${governmentType}.type" value="${parameter.selectedTypeHistory}"/>
            <spring-form:hidden path="${governmentType}.id" id="edit-action-val"/>
            <spring-form:hidden path="action" value="Add" id="action-val"/>
            <div class="form-group">
                <label class="col-sm-1 control-label" for="name">Name</label>
                <div class="col-sm-2">
                    <spring-form:input id="name" path="${governmentType}.name" type="text" class="form-control" placeholder="Name"/>
                    <spring-form:errors path="${governmentType}.name" cssClass="error-message"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-1 control-label" for="cemp">Employee Count</label>
                <div class="col-sm-2">
                    <spring-form:input id="cemp" path="${governmentType}.countOfEmployees" type="text" class="form-control" placeholder="Count of Employees"/>
                    <spring-form:errors path="${governmentType}.countOfEmployees" cssClass="error-message"/>
                </div>
            </div>
            <c:choose>
                <c:when test="${governmentType eq 'carProducer'}">
                    <div class="form-group">
                        <label class="col-sm-1 control-label" for="filials">Filials</label>
                        <div class="col-sm-2">
                            <spring-form:input id="filials" path="${governmentType}.rawOpenedFilials" type="text" class="form-control" placeholder="Opened Filials"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label" for="models">Best Models</label>
                        <div class="col-sm-2">
                            <spring-form:input id="models" path="${governmentType}.rawProducedModels" type="text" class="form-control" placeholder="Enabled Models"/>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="form-group">
                        <label class="col-sm-1 control-label" for="add-bank-datepicker">Date</label>
                        <div class="col-sm-2">
                            <spring-form:input id="add-bank-datepicker" path="${governmentType}.dateOfBase" type="text" class="form-control" placeholder="Birthday Date"/>
                            <spring-form:errors path="${governmentType}.dateOfBase" cssClass="error-message"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label" for="hm">Bosses</label>
                        <div class="col-sm-2">
                            <spring-form:input id="hm" path="${governmentType}.headMens" type="text" class="form-control" placeholder="Bosses names"/>
                            <spring-form:errors path="${governmentType}.headMens" cssClass="error-message"/>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>
            <div class="form-group col-sm-4">
                <spring-form:button type="submit" name="submit-butt" disabled="false" class="btn btn-success pull-right">Save</spring-form:button>
            </div>
        </spring-form:form>
    </div>
    <div/>
    <%--END TO SHOW GOVERNMENT--%>


<script type="text/javascript">

    jQuery(function ($) {
        $('#add-government-form').validate({
            rules: {
                '${governmentType}.name': {
                    required: true,
                    minlength: 3,
                    maxlength: 12
                },
                '${governmentType}.countOfEmployees': {
                    required: true,
                    maxlength: 2
                },
                '${governmentType}.rawOpenedFilials': {
                    required: true
                },
                '${governmentType}.rawProducedModels': {
                    required: true
                },
                '${governmentType}.dateOfBase': {
                    required: true
                },
                '${governmentType}.headMens': {
                    required: true,
                    minlength: 3,
                    maxlength: 25
                }
            },
            /*TODO move to message.properties*/
            messages: {
                '${governmentType}.name': {
                    required: "This field must not be empty",
                    minlength: "Username length must be more then 3 and less then 12 symbols",
                    maxlength: "Username length must be more then 3 and less then 12 symbols"
                },
                '${governmentType}.countOfEmployees': {
                    required: "This field must not be empty",
                    maxlength: "Max count must not be bigger then 100 employees"
                },
                '${governmentType}.rawOpenedFilials': {
                    required: "This field must not be empty"
                },
                '${governmentType}.rawProducedModels': {
                    required: "This field must not be empty"
                },
                '${governmentType}.dateOfBase': {
                    required: "This field must not be empty"
                },
                '${governmentType}.headMens': {
                    required: "This field must not be empty",
                    minlength: "Length must be more then 3 and less then 25 symbols",
                    maxlength: "Length must be more then 3 and less then 25 symbols"
                }
            },
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    });

    $(document).ready(function () {




//        $('#add-government-form').bootstrapValidator({
//            framework: 'bootstrap',
//            feedbackIcons: {
//                valid: 'glyphicon glyphicon-ok',
//                invalid: 'glyphicon glyphicon-remove',
//                validating: 'glyphicon glyphicon-refresh'
//            },
//            row: {
//                valid: 'field-success',
//                invalid: 'field-error'
//            },
//            fields: {
//                name: {
//                    validators: {
//                        notEmpty: {
//                            message: 'The name is required'
//                        },
//                        stringLength: {
//                            min: 6,
//                            max: 30,
//                            message: 'The username must be more than 6 and less than 30 characters long'
//                        },
//                        regexp: {
//                            regexp: /^[a-zA-Z0-9_\.]+$/,
//                            message: 'The username can only consist of alphabetical, number, dot and underscore'
//                        }
//                    }
//                },
//                cemp: {
//                    validators: {
//                        notEmpty: {
//                            message: 'The Employee Count is required'
//                        },
//                        stringLength: {
//                            max: 3,
//                            message: 'The Employee Count must be more then 100 employees'
//                        }
//                    }
//                },
//                filials: {
//                    validators: {
//                        notEmpty: {
//                            message: 'The Filials is required'
//                        }
//                    }
//                },
//                models: {
//                    validators: {
//                        notEmpty: {
//                            message: 'The Best Models is required'
//                        }
//                    }
//                },
//                datepicker: {
//                    validators: {
//                        notEmpty: {
//                            message: 'The Date is required'
//                        }
//                    }
//                },
//                hm: {
//                    validators: {
//                        notEmpty: {
//                            message: 'The Names of Bosses is required field'
//                        }
//                    }
//                }
//            }
//        });

        var validation = "${validationState}";
        if (validation == 'false'){
            showElement('add-form-${parameter.selectedTypeHistory}')
        }
    });
    $(function() {
        var d = new Date();

        var currDate = d.getDate();
        var currMonth = d.getMonth();
        var currYear = d.getFullYear();

        var dateStr = currDate + "-" + currMonth + "-" + currYear;

        $('#add-bank-datepicker').datepicker({
            format: 'dd-mm-yyyy',
            setDate: new Date(),
            autoclose: true
        });

    });

    function governmentEditingProcess(id, loopIndex) {

        if (id) {
            $('#action-val').val("Edit");
            $('#edit-action-val').val(id);
        }

        showElement('add-form-${parameter.selectedTypeHistory}')

        //Common part
        var name = $('#row-name-' + loopIndex).text();
        $('#name').val(name);
        var cOE = $('#row-countOfEmployees-' + loopIndex).text();
        $('#cemp').val(cOE);

        //Specific part
        var type = "${governmentType}";
        if(type == 'bankGovernment'){
            var formatDate = $('#row-formatDate-' + loopIndex).text();
            $('#add-bank-datepicker').val(formatDate);

            var rhm = $('#row-headMens-' + loopIndex).text();
            $('#hm').val(rhm);
        } else {
            if (type == 'carProducer'){

            }
        }
    }

</script>

<style type="text/css">
    .student-form-input{
        float: inherit;
    }

    /*GOVERNMENT CSS*/
    .selected{
        background-color: #040C04;
    }

    .gov-header{
        float: left;
        padding: 10px;
    }

    .header-container {
        height: 60px;
        margin: 15px 0 0 10px;
        padding-top: 10px;
        cursor: pointer;
        margin-right: 10px
    }

    .parameter-selecteddate-container span{
        color: red;
    }
    .add-form{
        display: none;
    }
    .form-wrapper {
        margin: 20px;
    }
    .error-message{
        color: red;
    }
</style>