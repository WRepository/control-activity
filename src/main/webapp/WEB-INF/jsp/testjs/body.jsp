<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div id="${categoryKeyId}" class="category-inner-item">

    <c:forEach var="appItem" items="${requestScope[categoryKeyId]}">
        <img class="category-image" src="/showImageCategory?imageId=${appItem.id}">
    </c:forEach>

    <div class="category-inner-body-item ">
        <form id="image-fm-${categoryKeyId}" method="post" action="${pageContext.request.contextPath}/addCategoryImage" enctype="multipart/form-data" class="addImageForm">
            <input type="text" value="${categoryKeyId}" hidden name="categoryKeyId">
            <input type="file" name="file">
            <input type="button" onclick="$('#image-fm-${categoryKeyId}').submit()" value="Send">
        </form>
    </div>
</div>