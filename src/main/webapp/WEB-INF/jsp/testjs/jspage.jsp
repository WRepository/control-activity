<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/headers.jsp"/>

<c:choose>
    <c:when test="${accessToPageDenied eq true}">
        <jsp:include page="../support/accessDenied.jsp"/>
    </c:when>

    <c:otherwise>
        <h1 style="padding-bottom: 40px">Photo Gallery</h1>

        <c:forEach items="${categoriesValues}" var="category" varStatus="status">
            <div class="category-item" onclick="showHideCategoryBlock('${category.key}')">${category.value}</div>
            <c:set var="categoryKeyId" value="${category.key}" scope="request"/>
            <jsp:include page="body.jsp"/>
        </c:forEach>
    </c:otherwise>
</c:choose>

<style>
    .category-item{
        background: #00BFF9;
        width: 100%;
        height: 30px;
        margin: 10px 10px 0 10px;
        cursor: pointer;
    }

    .category-inner-item{
        width: 100%;
        height: auto;
        min-height: 100px;
        border: 1px solid slateblue;
        margin: 0 10px 10px 10px;
        display: none;
        position: relative;
    }

    .category-inner-body-item {
        position: absolute;
        bottom: 0;
    }

    .category-image {
        max-height: 400px;
        margin: 15px 15px 50px 15px;
    }

</style>

<script type="text/javascript">

    function showHideCategoryBlock(divId) {
        var divId = '#'.concat(divId);

        var propertyCSSValue = $(divId).css('display');
        if (propertyCSSValue !='none') {
            $(divId).css({'display':'none'})
        } else {
            $(divId).css({'display':'block'})
        }

    }

</script>