<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="aui" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/headers.jsp"/>

<p><a href="javascript:;" onclick="showMiniature()">Show only miniature on page</a></p>
<table id="eatTable" class="display" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Date Of Birthday</th>
        <th>Image</th>
    </tr>
    </thead>
    <tbody>

    <c:set var="show" value="${user ne null}" scope="page"/>
    <c:if test="${show}">
        <c:forEach var="userPhoto" items="${user.userPhotos}" varStatus="status">
            <tr>
                <td>${user.id}</td>
                <td>${user.userName}</td>
                <fmt:formatDate pattern="yyyy-MM-dd " value="${user.dateBirthday != null ? user.dateBirthday: ''}" var="formatedDate"/>
                <td>${formatedDate}</td>
                <td>
                    <img id="intim-${user.id}" class="userImage" src="/showImage?imageId=${userPhoto.id}">
                </td>
            </tr>
        </c:forEach>
        <c:forEach var="newPhotoRow" begin="0" end="2" step="1">
            <tr>
                <td>${user.id}</td>
                <td>${user.userName}</td>
                <fmt:formatDate pattern="yyyy-MM-dd " value="${user.dateBirthday != null ? user.dateBirthday: ''}" var="formatedDate"/>
                <td>${formatedDate}</td>
                <td>
                    <form id="image-fm-${user.id}" method="post" action="${pageContext.request.contextPath}/addImage" enctype="multipart/form-data" class="addImageForm">
                        <input type="text" value="${user.id}" hidden name="userId">
                        <input type="file" name="file">
                        <input type="button" onclick="$('#image-fm-${user.id}').submit()" value="Send">
                    </form>
                </td>
            </tr>
        </c:forEach>
    </c:if>
    </tbody>
</table>


<script type="text/javascript">
    $(document).ready(function(){
        $('#eatTable').DataTable({
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "iDisplayLength": 25,
            "pageLength":15,
            "aaSorting":[]
        });
    });

    function showMiniature() {
        $('[id^="intim-"]').each(function () {
            $(this).css({
                "maxHeight": "100px"
            });
        });

        redrawDOM();
    }

    function redrawDOM() {
        $(window).trigger('resize');
    }
</script>
<style>
    .addImageForm{
        margin-bottom: 0px;
    }
    .userImage {
        max-height: 30%;
    }
</style>