<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<script src="${pageContext.request.contextPath}resources/bower_components/jquery/jquery.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}resources/bower_components/bootstrap/core/bootstrap.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}resources/bower_components/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}resources/bower_components/datatables.net-bs/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}resources/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}resources/bower_components/jquery-validation/jquery.validate.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}resources/external/additional-methods.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}resources/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}resources/static/formValidation.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}resources/static/bootstrap.min.js" type="text/javascript"></script>

<link href="<c:url value="${pageContext.request.contextPath}resources/bower_components/bootstrap/core/bootstrap.css"/>" type="text/css" rel="stylesheet"/>
<link href="<c:url value="${pageContext.request.contextPath}resources/bower_components/datatables.net-bs/dataTables.bootstrap.css"/>" type="text/css" rel="stylesheet"/>
<link href="<c:url value="${pageContext.request.contextPath}resources/bower_components/bootstrap-datepicker/bootstrap-datepicker3.css"/>" type="text/css" rel="stylesheet"/>
<link href="<c:url value="${pageContext.request.contextPath}resources/bower_components/bootstrap-datepicker/bootstrap-datepicker3.css"/>" type="text/css" rel="stylesheet"/>
<link href="<c:url value="${pageContext.request.contextPath}resources/static/formValidation.min.css"/>" type="text/css" rel="stylesheet"/>


<link href="<c:url value="${pageContext.request.contextPath}resources/bower_components/bootstrap/fonts/glyphicons-halflings-regular.woff2 "/>" />
<link href="<c:url value="${pageContext.request.contextPath}resources/bower_components/bootstrap/fonts/glyphicons-halflings-regular.woff "/>" />
<link href="<c:url value="${pageContext.request.contextPath}resources/bower_components/bootstrap/fonts/glyphicons-halflings-regular.ttf "/>" />






