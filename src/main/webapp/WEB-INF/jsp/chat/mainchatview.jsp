<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../headers.jsp"/>

<style type="text/css">
    .chat-room{
        height: 266px;
        width: 200px;
        overflow-y: scroll;
        border:1px solid black;
    }
    .container{
        position: relative;
        height: 400px;
        width: 600px;
        margin: auto auto;
    }
    .chat-room-element{
        padding: 15px 15px;
    }
</style>


<div class="lead">
    Testing Mediator Pattern
</div>

<div class="container">
    <div id="headers" class="col-xs-9 chat-room-element">
        <span style="text-decoration: underline"> Default chat room</span>
        <button class="btn btn-default" onclick="startChat()" type="button" name="start btn">Start chatting</button>
        <span style="color: red">HttpServletResponse from one user doesn't to use from another user, but object link correct :)</span>
    </div>

    <div id="chat-wrapper" class="col-xs-9 chat-room-element">
        <div id="chat-room" class="chat-room">
        </div>
    </div>

    <div id="add-new" class="col-xs-9 chat-room-element">

            <textarea class="form-control" rows="2" id="chat-message-container" name="addedTextBody" placeholder="Enter new message"></textarea>
            <button class="btn btn-primary" type="button" onclick="addChatMessage()">Send</button>

    </div>
</div>

<script type="text/javascript">

    function addChatMessage() {
        var msg = $('#chat-message-container').val();
        $('#chat-message-container').val("");
        $.post(
                {
                    url: '/addMessage',
                    data: {message: msg},
                    async: true,
                    success: function(msgToAdd, textStatus, jqXHR){
                        var respAsObj = $.parseJSON(msgToAdd);
                        var message = respAsObj.message;
                        var timestamp = respAsObj.timestamp;

                        $('#chat-room').append('<p>' +message + '<span style="float: right">' + timestamp + '</span>'+ '</p>');
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert(errorThrown);
                    }
                }
        );
    }

    function startChat() {
        $.get({
            url: '/startChat',
            async: true,
            success: function (msgToAdd, textStatus, jqXHR) {

            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });

    }
</script>