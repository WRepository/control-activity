<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <%--<div class="navbar-header">--%>
        <%--<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">--%>
        <%--<span class="sr-only">Toggle navigation</span>--%>
        <%--<span class="icon-bar"></span>--%>
        <%--<span class="icon-bar"></span>--%>
        <%--<span class="icon-bar"></span>--%>
        <%--</button>--%>
        <%--<a class="navbar-brand" href="#">Brand</a>--%>
        <%--</div>--%>
        <%--<jsp:useBean id="loggedUser" scope="application" type="com.vlad.home.model.User"/>--%>
        <p class="navbar-text">Signed in as <a href="#" class="navbar-link">${loggedUser.userName}</a></p>

        <p class="navbar-text">${loggedUser.dateBirthday}</p>

        <p class="navbar-text">${loggedUser.emailAdress}</p>

        <jsp:useBean id="applicationHeaders" scope="application" type="java.util.Map"/>
        <c:forEach items="${applicationHeaders}" var="currHeader">
            <p class="navbar-text">
                <a class="header" href="/${currHeader.key}">${currHeader.value}</a>
            </p>
        </c:forEach>

        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Actions<span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#">...</a></li>
                    <li role="separator" class="divider"></li>
                    <li>
                        <a href="<c:url value="/logout"/>">Выйти</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>


<style type="text/css">
    .navbar{
        background-color: #187D32;
    }

    .content-body span{
        font-size: 20px;
        margin: 10px;
        font-family: sans-serif;
    }

    .header {
        color: #96C3EA;
    }

</style>