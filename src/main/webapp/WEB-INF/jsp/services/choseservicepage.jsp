<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="${pageContext.request.contextPath}/WEB-INF/jsp/headers.jsp"/>



<form id="main-form" method="get" action="/processervice" class="form-horizontal">

    <%--<div class="col-md-4">--%>
        <%--Choose service:--%>
    <%--</div>--%>

    <div class="col-md-4">
        <div class="row">
            <div class="col-xs-4">
                <select class="form-control" name="selected-service">
                    <option value="email">Email Service</option>
                    <option value="sms">Sms Service</option>
                    <option value="mock">Mock Service</option>
                </select>
            </div>
            <div class="col-xs-1">
                <button type="submit" class="btn btn-primary" id="submit" name="submit">Start Service</button>
            </div>
        </div>
    </div>
</form>


<div class="inner-div">
    <button id="run-scheduled-btn" type="button" onclick="sendShedulerRequest()" class="btn btn-default">Run Email Scheduler</button>
    <span id="errorMsg"></span>
</div>
<div>
    <form id="chain-request" action="/runallservices" method="get">
        <button type="submit" class="btn btn-default">Launch services</button>
    </form>
</div>


<script type="text/javascript">
    function sendShedulerRequest() {
        $.ajax({
            type:"get",
            url:"/runservicewithscheduler",
            async: true,
            success: function (status) {
                var obj = $.parseJSON('{'+status+'}');
                showAlreadySchedules(obj);
            },
            error: function (exception) {
                debugger;
            }
        });
    }

    function showAlreadySchedules(st) {
        var msg = st.msg;
        $('#errorMsg').html(st.msg);
        if(st.status=='error'){
            $('#errorMsg').css({
               'color':'red'
            });
            $('#run-scheduled-btn').addClass("disabled")
        }
    }
</script>














<style type="text/css">
    .inner-div{
        margin-top: 200px;
    }
</style>